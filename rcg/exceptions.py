

class DuplicateNameError(Exception):
    """
    Two identical names are used where names must be unique.
    """
    def __init__(self, msg):
        super().__init__(self, msg)


class InvalidDataType(Exception):
    """
    An invalid data type was found
    """
    def __init__(self, msg):
        super().__init__(self, msg)