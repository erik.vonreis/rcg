"""
Global error collection and reporting
"""

import logging
from typing import List


_errors: List[str] = []

_warnings: List[str] = []


_error_context: List[str] = []


def add_context(msg: str) -> str:
    """
    Add the context to a message
    :param msg:
    :return:
    """
    if len(_error_context) == 0:
        return msg
    else:
        return f"{_error_context[-1]}: {msg}"


def report_error(msg: str) -> None:
    full_msg = add_context(msg)
    logging.error(full_msg)
    _errors.append(full_msg)
    if len(_errors) > 100:
        raise Exception("Maximum number of errors reached")


def report_warning(msg: str) -> None:
    full_msg = add_context(msg)
    logging.warning(full_msg)
    _warnings.append(full_msg)


def errors() -> List[str]:
    return _errors.copy()


def warning() -> List[str]:
    return _warnings.copy()


def error_count() -> int:
    return len(_errors)


def warning_count() -> int:
    return len(_warnings)


def push_error_context(context: str):
    _error_context.append(context)


def pop_error_context():
    _error_context.pop()


class ErrorContext:

    def __init__(self, context_description: str):
        self.context_description = context_description

    def __enter__(self):
        push_error_context(self.context_description)

    def __exit__(self, exc_type, exc_value, traceback):
        pop_error_context()
