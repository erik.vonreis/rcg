import sys
from pathlib import Path
from  typing import Type, TYPE_CHECKING

from rcg.compiler.compiler import Compiler, FatalCompilationError
from rcg.compiler.mdl.mdl_compiler import MDLCompiler
from rcg.errors import report_error, ErrorContext
from rcg.model.model_graph import ModelError

if TYPE_CHECKING:
    from rcg.model import Model


def build(model_name: str, compiler: Type[Compiler]) -> 'Model':
    compiler_instance = compiler(model_name)
    with ErrorContext("model_name"):
        try:
            model = compiler_instance.compile_model()
        except FatalCompilationError as e:
            report_error(f"Fatal compilation error: {str(e)}")
            raise e

        try:
            model.finalize_simple_graphs()
            model.flatten_graphs()
        except ModelError as e:
            report_error(f"Model error{str(e)}")
            raise e

    return model


def generate_sequence(model_name: str, base_path: str) -> None:
    model = build(model_name, MDLCompiler)
    sequence_file = Path(base_path) / Path(model_name) / Path("logs") / Path(f"{model_name}_partSequence.txt")
    model.generate_comparison_sequence_file(str(sequence_file))


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python build.py model_name base_path")
    else:
        model_name = sys.argv[1]
        base_path = sys.argv[2]
        generate_sequence(model_name, base_path)
