from typing import Optional, Dict, List, TYPE_CHECKING
from dataclasses import dataclass
import logging

from ..errors import report_error

if TYPE_CHECKING:
    from .model_graph import ModelGraph, ModelGraphs

@dataclass
class DAQChannel(object):
    """represents a (possible) DAQ channel or test point"""
    name: str
    rate: int


class Model:
    """
    An abstract representation of a model, including graph representations and other info: parameters, DAQ channels etc.
    """
    def __init__(self, rate_hz: int, parameters: Dict[str, str],
                 requested_daq_channels: List[DAQChannel], simple_graphs: "ModelGraphs"):
        """

        :param name: The name of the model
        :param rate_hz: The rate of the model in Hz
        :param parameters: The model parameters ad defined in the param block
        :param simple_graphs: A collection of simple graphs that make up the
        :param requested_daq_channels: A list of DAQ channels  requested in the DAQ channel block.
        """
        self.simple_graphs = simple_graphs

        # these graphs are copies of the simple_graphs with some bits filled in.  They are ready
        # to be added to the composite_graph
        self.finalized_simple_graphs: Optional["ModelGraphs"] = None

        # the final composite graph of the model, to be calculated later
        self.flattened_graphs: Optional["ModelGraphs"] = None

        self.parameters = parameters

        self.rate_hz = rate_hz

        self.requested_daq_channels = requested_daq_channels

    def finalize_simple_graphs(self):
        logging.info("Finalizing simple graphs")
        self.finalized_simple_graphs = self.simple_graphs.copy()
        self.finalized_simple_graphs.connect_goto_nodes()

    def flatten_graphs(self) -> None:
        """
        Flatten finalized simple graphs into a single composite graph

        :return:
        """
        if self.finalized_simple_graphs is None:
            report_error("flatten_graphs() called before simple graphs were finalized")
            return

        self.flattened_graphs = self.finalized_simple_graphs.flatten()
        # this should never fail, since there's no way to create a loop between graphs without
        # creating a loop in a simple graph, checked in the finalize stage
        if self.flattened_graphs.top_graph().check_for_cycles((self.flattened_graphs.model_name, )):
            raise Exception("RCG error: compsite graph should not contain cycles that weren't caught in the simple graphs")

    def generate_comparison_sequence_file(self, path):
        if self.flattened_graphs is None:
            raise Exception("RCG bug: generate_comparison_Sequence_file should not be called before flatten_graphs")
        top_graph = self.flattened_graphs.top_graph()
        sequence = top_graph.get_sequence()
        with open(path, 'w') as f:
            for node in sequence:
                part_prefix = top_graph.nodes[node]['part_prefix']
                part = top_graph.part_from_node(node)
                if part.calculable:
                    f.write(f"{node},{part_prefix.plus_channel(part.name)}\n")


