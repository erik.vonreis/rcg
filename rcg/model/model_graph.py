import networkx
from networkx import MultiDiGraph, nx_agraph, set_node_attributes, relabel_nodes, find_cycle, NetworkXNoCycle, topological_sort
from typing import TypeAlias, Tuple, Dict, List, Any, Optional, TYPE_CHECKING
from enum import Enum
import logging

from ..errors import report_error, report_warning, ErrorContext
from ..parts import From, Goto, Delay, Subsystem, Inport, Outport

if TYPE_CHECKING:
    from rcg.parts.part import Part
    from ..compiler.compiler import Parameters


# these are used to memoize certain nodes that need special graph processing.
# tracked so we don't have to iterate the graph over and over again
class SpecialNodeTypes(Enum):
    FROM = 0,
    TO = 1,
    SUBSYSTEM = 2,
    DELAY = 3,


RESERVED_NODE_ARGS = "part in_ports out_ports".split()


class ModelError(Exception):
    """Raised if there was problem detected in the model design"""


class ModelGraph(MultiDiGraph):
    """
    Represents the graph structure of all or part of a model
    """
    def __init__(self):
        super().__init__()

        # special nodes memoized here to speed up processing later
        self.goto_nodes: Dict[str, int] = {}
        self.from_nodes: List[int] = []
        self.delay_nodes: List[int] = []

        # these are the outside ports of a subsystem
        self.subsystem_nodes: List[int] = []

        # these are the inside ports of a subsystem
        self.inport_nodes: Dict[int, int] = {}
        self.outport_nodes: Dict[int, int] = {}

        self.parameters: Optional[Parameters] = None

        self.node_by_name: Dict[str, int] = {}

        self.node_by_part: Dict[Part, int] = {}

        self.highest_node = -1

    def copy(self, as_view=False) -> "ModelGraph":
        new_graph: ModelGraph = super().copy(as_view=as_view)
        new_graph.goto_nodes = self.goto_nodes.copy()
        new_graph.from_nodes = self.from_nodes.copy()
        new_graph.delay_nodes = self.delay_nodes.copy()
        new_graph.subsystem_nodes = self.subsystem_nodes.copy()
        new_graph.inport_nodes = self.inport_nodes.copy()
        new_graph.outport_nodes = self.outport_nodes.copy()
        if self.parameters is not None:
            new_graph.parameters = self.parameters.copy()
        else:
            new_graph.parameters = None
        new_graph.node_by_name = self.node_by_name.copy()
        new_graph.node_by_part = self.node_by_part.copy()
        new_graph.highest_node = self.highest_node

        return new_graph

    def add_node(self, node, **kwargs):
        """
        Track the highest node
        :param node:
        :param kwargs:
        :return:
        """
        super().add_node(node, **kwargs)
        if node > self.highest_node:
            self.highest_node = node

    def add_part(self, part: "Part", node_args: Optional[Dict[str, Any]] = None):
        """
        Add a part to the graph
        :param part:
        :param node_args: Optional additional node arguments to add to the graph.
        :return:
        """
        global RESERVED_NODE_ARGS
        if node_args is None:
            node_args_dict: Dict[str, Any] = {}
        else:
            node_args_dict = node_args
            for arg in node_args:
                if arg in RESERVED_NODE_ARGS:
                    raise KeyError(f"node_args passed reserved argument '{arg}'")
        if part in self.node_by_part:
            report_error(f"The part named {part.name} was added twice to the same graph")
            return
        if part.name in self.node_by_name:
            orig_node = self.node_by_name[part.name]
            orig_part = self.part_from_node(orig_node)
            msg = f"""two parts with the same name '{part.name}'  were added to a graph
    Part 1 is {orig_part}
    Part 2 is {part}
    Keeping Part 1 and ignoring Part 2"""
            report_error(msg)
        else:
            new_node = self.highest_node + 1
            self.node_by_part[part] = new_node
            self.node_by_name[part.name] = new_node
            self.add_node(new_node, part=part, name=part.name, in_ports=part.in_ports,
                          out_ports=part.out_ports, **node_args_dict)

            # Handle some special node types and
            # memoize special node types for later handling
            match part:
                case Goto():
                    if part.goto_tag in self.goto_nodes:
                        other_name = self[self.goto_nodes[part.goto_tag]]['name']
                        report_error(f"""Goto tag {part.goto_tag} appears twice in parts 
    '{other_name}' and '{part.name}'""")
                    else:
                        self.goto_nodes[part.goto_tag] = new_node
                case From(): self.from_nodes.append(new_node)
                case Delay():
                    self.delay_nodes.append(new_node)
                    src_node = self.highest_node+1
                    self.add_node(src_node, part=part.delay_output, name=part.name, in_ports=part.delay_output.in_ports,
                                  out_ports=part.delay_output.out_ports,
                                  **node_args_dict)
                    set_node_attributes(self, {new_node: src_node}, "source_node")
                case Subsystem(): self.subsystem_nodes.append(new_node)
                case Inport(): self.inport_nodes[part.port_num] = new_node
                case Outport(): self.outport_nodes[part.port_num] = new_node
                case _: pass  # don't memoize other types

    def get_source_node_by_name(self, name: str) -> int:
        """Given a part name, return the part object suitable for an edge source
        """
        node = self.node_by_name[name]
        if 'source_node' in self.nodes[node]:
            return self.nodes[node]['source_node']
        return node

    @staticmethod
    def check_source_port(source_part: 'Part', port_num: int) -> bool:
        """
        Return False and report an error if there's a problem with the output port number on the given part
        :param source_part:
        :param port_num:
        :return:
        """
        if len(source_part.out_ports) < port_num:
            report_error(f"Output port number {port_num} on part '{source_part.name}'" 
                         f" greater than the number of ports ({len(source_part.out_ports)})")
            return False
        return True

    @staticmethod
    def check_dest_port(dest_part: 'Part', port_num: int) -> bool:
        """
        Return False and report an error if there's a problem with the output port number on the given port
        :param dest_part:
        :param port_num:
        :return:
        """
        if len(dest_part.in_ports) < port_num:
            report_error(f"Input port number {port_num} on part '{dest_part.name}'" 
                         f" greater than the number of ports ({len(dest_part.in_ports)})")
            return False
        if dest_part.in_ports[port_num - 1].linked:
            report_error(f"Part '{dest_part.name}' input port {port_num} was linked to multiple times")
            return False
        return True

    def part_from_node(self, node: int) -> 'Part':
        return self.nodes[node]['part']

    def add_link(self, src_part_name: str, src_port: int, dest_part_name: str, dest_port: int):
        """

        :param src_part_name: Name of part/node that's the source of the edge
        :param src_port: 0-based output port number
        :param dest_part_name: Name of part/node that's the destination of the edge
        :param dest_port: 0-based input port number
        :return:
        """
        source_node = self.get_source_node_by_name(src_part_name)
        source_part = self.part_from_node(source_node)
        if not self.check_source_port(source_part, src_port):
            return
        dest_node = self.node_by_name[dest_part_name]
        dest_part = self.part_from_node(dest_node)
        if not self.check_dest_port(dest_part, dest_port):
            return
        self.add_edge(source_node, dest_node, src_port=src_port, dest_port=dest_port)

    def write_dot(self, fname):
        names: Dict[int, str] = {}
        for node, attributes, in self.nodes.items():
            names[node] = attributes['name'] + f"({node})"
        named_g = relabel_nodes(self, names)
        nx_agraph.write_dot(named_g, fname)

    def connect_goto_nodes(self):
        """
        Add edges between Goto nodes and associated From nodes

        This mutates the graph, so use with caution
        :return:
        """
        unusued_goto_tags = set(self.goto_nodes.keys())
        for from_node in self.from_nodes:
            from_predecessors = self.predecessors(from_node)
            from_part = self.part_from_node(from_node)

            try:
                from_predecessors.__next__()
                # there should be no predecessors
                # so StopIteration should be thrown
                report_error(f"From node named '{from_part.name}' was matched with two different Goto tags."
                             "  This is a bug in the compiler.")
                continue
            except StopIteration:
                pass

            if not isinstance(from_part, From):
                report_error(f"A part, '{from_part.name}', was recognized as a From tag but was not a From part.")
                continue
            goto_tag = from_part.goto_tag
            if goto_tag not in self.goto_nodes:
                report_error(f"Could not find a Goto Tag with tag '{goto_tag}' to match From Tag, '{from_part.name}'")
                continue
            if goto_tag in unusued_goto_tags:
                unusued_goto_tags.remove(goto_tag)

            goto_node = self.goto_nodes[goto_tag]
            goto_part = self.part_from_node(goto_node)
            self.add_link(goto_part.name, 1, from_part.name, 1)

        if len(unusued_goto_tags) > 0:
            tags = ",".join(unusued_goto_tags)
            report_warning(f"Some Goto Tags did not have corresponding From Tags: {tags}")

    def union(self, other: 'ModelGraph',
              channel_prefix: 'ChannelPrefix', part_prefix: 'PartNamePrefix') -> Tuple['ModelGraph', int,]:
        """
        This is like a networkX disjoint union, but preserves the node ids for self
        :param other:
        :param channel_prefix: channel prefix for the new subsystem
        :param part_prefix: part prefix for the new subsystem
        :return:  the union model and difference in node numbers for the 'other' graph
        """
        union = self
        diff = self.highest_node + 1
        for node in other.nodes.keys():

            attribs = other.nodes[node].copy()
            if 'channel_prefix' in attribs:
                attribs['channel_prefix'] = channel_prefix + attribs['channel_prefix']
            else:
                attribs['channel_prefix'] = channel_prefix
            if 'part_prefix' in attribs:
                attribs['part_prefix'] = part_prefix + attribs['part_prefix']
            else:
                attribs['part_prefix'] = part_prefix

            union.add_node(node + diff, **attribs)

        for node in other.nodes.keys():
            adj = other.adj[node]
            for dest_node, edges in adj.items():
                for edge in edges.values():
                    union.add_edge(node + diff, dest_node + diff, **edge)
        return union, diff

    def join(self, subsys_node: int, sub_graph: 'ModelGraph',
             channel_prefix: 'ChannelPrefix', part_prefix: 'PartNamePrefix') -> None:
        """
        destructively adds of a subgraph to fill in a subsystem
        :param subsys_node: node of subsystem that sub_graph will be inserted into
        :param sub_graph: graph to insert
        :param channel_prefix: channel prefix for the new subsystem
        :param part_prefix: part prefix for the new subsystem
        :return:
        """

        inport_nodes = sub_graph.inport_nodes.copy()
        outport_nodes = sub_graph.outport_nodes.copy()

        # indexes of sub_graph nodes will be increaed by length(self)

        # new_graph = disjoint_union(self, sub_graph)

        new_graph, offset = self.union(sub_graph, channel_prefix, part_prefix)

        for pn in inport_nodes:
            inport_nodes[pn] += offset

        for pn in outport_nodes:
            outport_nodes[pn] += offset

        new_graph.copy_inport_edges(subsys_node, inport_nodes)
        new_graph.copy_outport_edges(subsys_node, outport_nodes)

        new_graph.remove_node(subsys_node)

    def copy_inport_edges(self, subsys_node: int, inport_nodes: Dict[int, int]) -> None:
        """
        Copy source edges going into a subsystem
        :param node:
        :return:
        """
        for pred_node in self.predecessors(subsys_node):
            edges = self.adj[pred_node][subsys_node]
            for edge in edges.values():
                port = edge['dest_port']
                inport_node = inport_nodes[port]
                inport_part = self.part_from_node(inport_node)
                if not isinstance(inport_part, Inport):
                    report_error(f"Compiler bug: looking for an Inport but got part '{inport_part.name}',"
                                 f" which is a '{type(inport_part)}'")
                    continue
                if inport_part.port_num != port:
                    report_error(f"Compiler bug: looking for inport for port {port} but"
                                 f" got {inport_part.port_num} from '{inport_part.name}'")
                    continue
                src_port = edge['src_port']
                self.add_edge(pred_node, inport_node, src_port=src_port, dest_port=1)

    def copy_outport_edges(self, subsys_node: int, outport_nodes: Dict[int, int]) -> None:
        out_edges = self.adj[subsys_node]
        for dest_node, edges in out_edges.items():
            for edge in edges.values():
                port = edge['src_port']
                outport_node = outport_nodes[port]
                outport_part = self.part_from_node(outport_node)
                if not isinstance(outport_part, Outport):
                    report_error(f"Compiler bug: looking for an Outport but got part '{outport_part.name}',"
                                 f" which is a '{type(outport_part)}'")
                    continue
                if outport_part.port_num != port:
                    report_error(f"Compiler bug: looking for outport for port {port} but"
                                 f" got {outport_part.port_num} from '{outport_part.name}'")
                    continue
                dest_port = edge['dest_port']
                self.add_edge(outport_node, dest_node, src_port=1, dest_port=dest_port)

    def check_for_cycles(self, key: 'GraphKey') -> bool:
        """
        Returns false if there are no cycles.  Otherwise, return True and report an error.
        :param key: needed for error reporting
        :return: True if there are cycles.
        """
        try:
            loop = find_cycle(self)
            key_name = "/".join(key)
            msg = f"A cycle was found in {key}:\n"
            nodes = [loop[0][0]] + [l[1] for l in loop]
            part_names = [self.part_from_node(n).name for n in nodes]
            loop_desc = " -> ".join(part_names)
            msg += f"\t{loop_desc}"
            report_error(msg)
            return True
        except NetworkXNoCycle:
            return False

    def get_sequence(self) -> List[int]:
        """
        Return a parts list of parts in topological order, that is, a valid computation sequence.
        :return:
        """
        return list(topological_sort(self))


GraphKey: TypeAlias = Tuple[str, ...]


class PartNamePrefix(tuple):
    """Represents the channel prefix for a particular Part, or more likely a graph of Parts"""

    def __new__(cls, *args: Tuple[str]):
        return super().__new__(cls, args)

    def __str__(self):
        return "_".join(self)

    def plus_channel(self, part_name: str) -> str:
        """
        Produce a string with the prefix plus an appended channel name
        :param part_name:
        :return:
        """
        return "_".join(self + (part_name,))

    def __add__(self, other: "PartNamePrefix") -> "PartNamePrefix":
        return PartNamePrefix(*super().__add__(other))


class ChannelPrefix(tuple):
    """Represents the channel prefix for a particular Part, or more likely a graph of Parts"""

    def __new__(cls, *args):
        x = super().__new__(cls, args)
        x.top_name = False
        return x

    def __str__(self):
        if len(self) > 2:
            return "-".join(self[:2]) + "_" + "_".join(self[2:])
        else:
            return "-".join(self)

    def plus_channel(self, channel_name: str) -> str:
        """
        Produce a string with the prefix plus an appended channel name
        :param channel_name:
        :return:
        """
        return str(self) + "_"+ channel_name

    def __add__(self, other: 'ChannelPrefix') -> 'ChannelPrefix':
        return ChannelPrefix(*super().__add__(other))


class ModelGraphs(object):
    """
    A collection of simple, single-system graphs that represent a model

    There will be a top system graph.  Other graphs will be subsystems
    referenced by the top graph
    """
    def __init__(self, model_name: str):
        """

        :param model_name: The name of the model.  Top graph is keyed by (model_name, )
        """
        self.model_name = model_name
        self.graphs: Dict[GraphKey, ModelGraph] = {}

    def top_graph(self) -> ModelGraph:
        """
        Return the top graph of a model
        :return:
        """
        return self.graphs[(self.model_name, )]

    def get_item(self, item: GraphKey) -> ModelGraph:
        """
        Get the model graph identified by the given key

        :param item:
        :return:
        """
        return self.graphs[item]

    def set_item(self, key: GraphKey, graph: ModelGraph) -> None:
        """
        Set the given key to point at the given graph.

        It's an error if the key already exists.
        :param key:
        :param graph:
        :return:
        """
        if key in self.graphs:
            raise KeyError("Duplicate keys cannot be added to a ModelGraphs object")
        self.graphs[key] = graph

    def __getitem__(self, item: GraphKey) -> ModelGraph:
        return self.get_item(item)

    def __setitem__(self, key: GraphKey, value: ModelGraph) -> None:
        """
        Set the given key to point at the given ModelGraph

        It's an error if the key already exists

        :param key:
        :param value:
        :return:
        """
        return self.set_item(key, value)

    def __contains__(self, item) -> bool:
        return item in self.graphs

    def connect_goto_nodes(self):
        """
        Add edges between Goto nodes for all graphs.  This mutates the graphs

        :return:
        """
        for key, graph in self.graphs.items():
            graph.connect_goto_nodes()
            if graph.check_for_cycles(key):
                raise ModelError("There were cycles in a model.  See error messages for details.")

    def copy(self):
        new_graphs = ModelGraphs(self.model_name)
        for key, graph in self.graphs.items():
            new_graphs.graphs[key] = graph.copy()
        return new_graphs

    def flatten(self) -> 'ModelGraphs':
        """
        Flatten the simple graphs into a collection of flattened graphs
        the top graph will be the graph of the whole model
        :return:
        """
        flattened_graphs = ModelGraphs(self.model_name)
        flattened_graphs.flatten_graph((self.model_name, ), self)

        # add to channel names if top systems aren't top named
        sys_name = ChannelPrefix(self.model_name[2:5])
        top_graph = flattened_graphs.top_graph()

        # massage channel and part names for the composite graph
        for node in top_graph.nodes:
            attribs = top_graph.nodes[node]
            if 'channel_prefix' in attribs:
                cp: ChannelPrefix = attribs['channel_prefix']
                if not cp.top_name:
                    set_node_attributes(top_graph, {node: sys_name + cp}, 'channel_prefix')
            else:
                set_node_attributes(top_graph, {node: sys_name}, 'channel_prefix')
            empty_prefix = PartNamePrefix()
            if 'part_prefix' not in attribs:
                set_node_attributes(top_graph, {node: empty_prefix}, 'part_prefix')

        return flattened_graphs

    def flatten_graph(self, key: GraphKey, source_graphs: 'ModelGraphs') -> None:
        key_name = "/".join(key)
        with ErrorContext(f"Flattening {key_name}"):
            logging.info(f"Filling in subsystems for {key_name}")
            if key in self.graphs:
                return
            new_graph = source_graphs[key].copy()
            for subsys_node in new_graph.subsystem_nodes:
                subsys = new_graph.part_from_node(subsys_node)
                if not isinstance(subsys, Subsystem):
                    report_error(f"Part {subsys.name} was compiled as a subsystem but is of type {type(subsys)}")
                    continue
                if subsys.graph_key is None:
                    report_error(f"Subsystem {subsys} did not have a graph key")
                    continue
                if "top_names" in subsys.tags:
                    logging.info(f"{subsys.name} is top named")
                    new_channel_prefix = ChannelPrefix(subsys.name[:3])
                    new_channel_prefix.top_name = True
                else:
                    new_channel_prefix = ChannelPrefix(subsys.name)
                new_part_prefix = PartNamePrefix(subsys.name)
                self.flatten_graph(subsys.graph_key, source_graphs)
                skey = "/".join(subsys.graph_key)
                logging.info(f"Inserting subsystem {skey} into {key_name} at {subsys.name}")
                new_graph.join(subsys_node, self.graphs[subsys.graph_key].copy(), new_channel_prefix, new_part_prefix)

            self.graphs[key] = new_graph
