from unittest import TestCase, mock, main
import os
from pathlib import Path
from ..compiler.mdl.mdl_compiler import MDLCompiler
from ..errors import error_count, warning_count
import logging
import shutil

script_dir = Path(__file__).parent
model_path = script_dir / Path("../../models")

class TestModelGraphs(TestCase):
    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def setUp(self):
        logging.basicConfig(level=logging.INFO)
        mc = MDLCompiler("h1tcscs")
        self.model = mc.compile_model()

    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def test_goto_tags_model(self):
        self.assertIsNotNone(self.model)
        self.model.simple_graphs.connect_goto_nodes()
        self.assertEqual(0, warning_count())
        self.assertEqual(0, error_count())

    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def test_finalize_simple_graphs(self):
        self.model.finalize_simple_graphs()
        self.assertEqual(0, warning_count())
        self.assertEqual(0, error_count())

    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def test_flatten(self):
        self.model.finalize_simple_graphs()
        self.model.flatten_graphs()
        self.assertIsNotNone(self.model.flattened_graphs)
        self.assertEqual(0, warning_count())
        self.assertEqual(0, error_count())
        self.model.flattened_graphs.top_graph().write_dot("/tmp/flat.dot")

class TestFinalModelGraph(TestCase):
    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def setUp(self):
        logging.basicConfig(level=logging.INFO)
        mc = MDLCompiler("h1tcscs")
        self.model = mc.compile_model()
        self.model.finalize_simple_graphs()
        self.model.flatten_graphs()
        self.model.flattened_graphs.top_graph().write_dot("/tmp/flat.dot")

    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def test_sequence(self):
        shutil.copy("models/sequences/h1tcscs_partConnectionList.txt",
                    "/tmp/h1tcscs_partConnectionList.txt")
        self.model.generate_comparison_sequence_file("/tmp/h1tcscs_partSequence.txt")


if __name__ == "__main__":
    main()