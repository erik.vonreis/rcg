from unittest import TestCase, mock, main
import os
from pathlib import Path

from ..model_file_search import model_file_search

script_dir = Path(__file__).parent
model_path = script_dir / Path("../../../models")


class TestModelFileSearch(TestCase):
    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def test_model_find(self):
        file_path = model_file_search("h1tcscs.mdl")
        self.assertIsNotNone(file_path)

if __name__ == "__main__":
    main()