from typing import TYPE_CHECKING, List
from dataclasses import dataclass
import logging

from .item import MDLObject, MDLObjectList, check_one_of, MDLString, MDLInt
from ...errors import report_error
from ..compiler import FatalCompilationError

if TYPE_CHECKING:
    from ...model import ModelGraph


def handle_lines(graph: 'ModelGraph', lines: List[MDLObject]) -> None:
    """
    Add line objects to the graph
    :param graph:
    :param lines:
    :return:
    """
    for line in lines:
        handle_line(graph, line)


@dataclass(frozen=True)
class Locus:
    block: str
    port: int


class Line(object):
    def __init__(self, source_block: str, source_port: int):
        self.source = Locus(source_block, source_port)
        self.destinations: List[Locus] = []

    def __str__(self):
        return f"{self.source}, {self.destinations}"

    def add_destination(self, dest_block, dest_port):
        self.destinations.append(Locus(dest_block, dest_port))

    def find_destinations(self, obj_dict):
        # get any top level destinations
        if 'DstBlock' in obj_dict:
            try:
                dst_block = check_one_of(obj_dict, 'DstBlock', MDLString, str)
                dst_port = check_one_of(obj_dict, 'DstPort', MDLInt, int)
            except Exception as e:
                raise FatalCompilationError(f"Badly formed Line object: {str(e)}")
            self.add_destination(dst_block, dst_port)
        branches = obj_dict.get('Branch', None)
        if branches is not None:
            for branch in branches:
                if not isinstance(branch, MDLObjectList):
                    raise FatalCompilationError("Branch object is not a compound object")
                self.find_destinations(branch.as_dict())

    def add_to_graph(self, graph: 'ModelGraph') -> None:
        """
        Add the line as a set of edges to the graph
        :param graph:
        :return:
        """
        for dest in self.destinations:
            graph.add_link(self.source.block, self.source.port, dest.block, dest.port)


def handle_line(graph: 'ModelGraph', mdl_line: MDLObject) -> None:
    if not isinstance(mdl_line.value, MDLObjectList):
        report_error("Line object is not a compound object")
        return None
    obj_dict = mdl_line.value.as_dict()
    try:
        src_block = check_one_of(obj_dict, 'SrcBlock', MDLString, str)
        src_port = check_one_of(obj_dict, 'SrcPort', MDLInt, int)
    except Exception as e:
        raise FatalCompilationError(f"Badly formed Line object: {str(e)}")

    line = Line(src_block, src_port)

    line.find_destinations(obj_dict)

    line.add_to_graph(graph)
