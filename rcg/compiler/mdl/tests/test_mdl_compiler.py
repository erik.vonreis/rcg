from unittest import TestCase, mock, main
import os
from pathlib import Path

from ..mdl_compiler import MDLCompiler
from ....errors import errors

script_dir = Path(__file__).parent
model_path = script_dir / Path("../../../../models")


class TestMDLCompiler(TestCase):
    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def test_compile_model(self):
        mc = MDLCompiler("h1tcscs")
        mc.compile_model()
        self.assertEqual(len(errors()), 0)

    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def test_compile_iop_model(self):
        mc = MDLCompiler("h1ioplsc0")
        mc.compile_model()
        self.assertEqual(len(errors()), 0)

    @mock.patch.dict(os.environ, {"RCG_LIB_PATH": str(model_path)})
    def test_compile_unittest_model(self):
        mc = MDLCompiler("x1unittest")
        mc.compile_model()
        self.assertEqual(len(errors()), 0)


if __name__ == "__main__":
    main()