#!env python3
# return a parse tree of mdl files as an MDLObjectList instance.

import ply.lex as lex
import ply.yacc as yacc
import sys
from .item import MDLWord, MDLObject, MDLObjectList, MDLString, MDLInt, MDLFloat, MDLExp


class MDLParserException(Exception):
    pass


# lexer rules

tokens = """WORD STRING FLOAT EXP INT O_CURLY C_CURLY O_SQUARE C_SQUARE COMMA SEMI"""
tokens = tokens.split()

t_WORD      = r'[$a-zA-Z_][.a-zA-Z0-9_]*'
t_STRING    = r'"([^"\\]|\\.)*"'
t_FLOAT     = r'[-+]?[0-9]+\.[0-9]+'
t_EXP       = r'[-+]?[0-9]+(\.[0-9]+)?[eE][-+]?[0-9]+'
t_INT       = r'[-+]?[0-9]+'
t_O_CURLY   = r'{'
t_C_CURLY   = r'}'
t_O_SQUARE  = r'\['
t_C_SQUARE  = r'\]'
t_COMMA     = r','
t_SEMI      = r';'

t_ignore = " \t"


def t_error(t):
    msg = f"Illegal character at {t.lineno}:{t.lexpos - t.lexer.line_start} '{t.value[0]}'"
    if t.lexer.raise_exception:
        raise MDLParserException(msg)
    else:
        sys.stderr.write(msg + "\n")


def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)
    t.lexer.line_start = t.lexpos + len(t.value) -1


lexer = lex.lex()
lexer.raise_exception = False
lexer.line_start=-1

# parser rules


def prepend_to_array(x, a):
    """prepend x onto a, then return the result.  does not affect a.

    works with nested arrays, so that 1 prepended to [[2,3],[4,5,6]] is prepended as [[1,2,3],[4,5,6]]
    """
    if len(a) == 0:
        return [x]

    if isinstance(a[0], list):
        na = [ prepend_to_array(x, a[0]) ] + a[1:]
        return na

    return [x] + a


def p_objectlist_object(t):
    'objectlist : object objectlist'
    t[0] = MDLObjectList([t[1]] + t[2])


def p_objectlist_end(t):
    'objectlist : '
    t[0] = MDLObjectList([])


def p_object_value(t):
    'object : WORD value'
    t[0] = MDLObject(t[1], t[2])


def p_value_word(t):
    'value : WORD'
    t[0] = MDLWord(t[1])


def p_value_number(t):
    'value : number'
    t[0] = t[1]


def p_value_string(t):
    'value : nonemptystringlist'
    t[0] = t[1]


def p_value_array(t):
    'value : O_SQUARE array C_SQUARE'
    t[0] = t[2]


def p_value_compound(t):
    'value : O_CURLY objectlist C_CURLY'
    t[0] = t[2]


def p_number_int(t):
    'number : INT'
    t[0] = MDLInt(t[1])


def p_number_float(t):
    'number : FLOAT'
    t[0] = MDLFloat(t[1])


def p_number_exp(t):
    'number : EXP'
    t[0] = MDLExp(t[1])


def p_nonemptystringlist_string(t):
    'nonemptystringlist : STRING stringlist'
    t[0] = MDLString(t[1][1:-1]) + t[2]


def p_stringlist_string(t):
    'stringlist : STRING stringlist'
    t[0] = MDLString(t[1][1:-1]) + t[2]


def p_stringlist_empty(t):
    'stringlist : '
    t[0] = MDLString()


def p_array_number(t):
    'array : number arraytail'
    t[0] = prepend_to_array( t[1], t[2])


def p_array_empty(t):
    'array : '
    t[0] = []


def p_arraytail_number(t):
    'arraytail : COMMA number arraytail'
    t[0] = prepend_to_array(t[2], t[3])


def p_arraytail_newrow(t):
    'arraytail : SEMI number arraytail'
    #check if already nested list
    n = t[2]
    if len(t[3]) == 0:
        t[0] = [[], [ n ]]
    elif isinstance(t[3][0], list):
        t[0] = [[]] + prepend_to_array(n, t[3])
    else:
        t[0] = [[], prepend_to_array(n, t[3])]


def p_arraytail_end(t):
    'arraytail : '
    t[0] = []


def p_error(t):
    errpos = t.lexpos - t.lexer.line_start
    msg = f'syntax error at line {t.lexer.lineno} col {errpos}: {t.value}'
    if t.lexer.raise_exception:
        raise MDLParserException(msg)
    else:
        sys.stderr.write(msg + "\n")


parser = yacc.yacc()


def parse_mdl_string(mdl_contents: str) -> MDLObjectList:
    """
    Return a parse tree of the contents of an mdl file_name

    :param mdl_contents: A string.  Contents of an mdl file_name to parse.
    :return: An MDL_Object instance that is the root object of the file_name.
    """
    global parser, lexer
    try:
        root_obj = parser.parse(mdl_contents)
    finally:
        lexer.lineno = 1
    return root_obj


def parse_mdl_file(mdl_filepath: str) -> MDLObjectList:
    """
    Return a parse tree of the contents of an mdl file_name

    :param mdl_filepath: A string.  Path to an MDL file_name.
    :return: An MDL_Object instance that is the root object of the file_name.
    """
    with open(mdl_filepath) as f:
        mdl_contents = f.read()
        return parse_mdl_string(mdl_contents)


def raise_exception_on_error(raise_exception):
    """
    Set to true if parser should raise an exception on an error.  Otherwise, error is printed to std out.

    :param raise_exception:
    :return:
    """
    global lexer
    lexer.raise_exception = raise_exception


def main():
    """
    If run from_part command line
    :return:
    """
    if len(sys.argv) != 2:
        print("pass mdl file_name names to parse")
        sys.exit(1)

    root_obj = parse_mdl_file(sys.argv[1])
    sys.stdout.write(str(root_obj))

if __name__ == "__main__":
    main()
