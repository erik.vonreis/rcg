from typing import Dict, Callable, List, TYPE_CHECKING, Optional
from dataclasses import dataclass
import logging

from .verify_parts import verify_parts
from ..compiler import Compiler, FatalCompilationError, convert_parameter_list
from ...model import Model, ModelGraphs, GraphKey, ModelGraph
from ..model_file_search import model_file_search
from ...errors import report_error, error_count, ErrorContext
from .parser import parse_mdl_file
from .item import MDLObject, MDLObjectList, MDLItem, check_one_of_convertable, MDLObjectListDictionary
from .object_fingerprint import fingerprint_object, ObjectFingerprint
from ...parts.part import Part
from ...parts import parts, Subsystem, SubsystemDisposition
from .lines import handle_lines

if TYPE_CHECKING:
    from mdl_handler_protocol import MDLHandlerProtocol

# memoize lookup of part by fp


@dataclass
class ProcessStackItem:
    """
    For tracking systems to be compiled
    """
    graph_key: GraphKey
    add_subsystems: bool  # when True, add external references to the stack

    def __repr__(self):
        key = "/".join(self.graph_key)
        return f"({key}, {self.add_subsystems})"


class MDLCompiler(Compiler):
    def __init__(self, model_name: str):
        self.parts_compilable = verify_parts()

        self.model_graphs = ModelGraphs(model_name)
        self.process_stack: List[ProcessStackItem] = [ProcessStackItem((model_name,), True)]

        # already parsed MDL items that may later be compiled
        self.item_refs: Dict[GraphKey, MDLObjectList] = {}

        # memoization of fp function
        self.fingerprint_memo: Dict[ObjectFingerprint, 'MDLHandlerProtocol'] = {}

        # default block parameters are on a per-file basis
        # self.default_block_parameters[mdl_name][block_type][param_name] = MDLItem
        self.default_block_parameters: Dict[str, Dict[str, MDLObjectListDictionary]] = {}

    def __iter__(self):
        return self

    def compile_step(self) -> None:
        ps_item = self.process_stack.pop()
        next_key = ps_item.graph_key
        add_subsystems = ps_item.add_subsystems
        key_name = "/".join(next_key)
        with ErrorContext(key_name):
            # the object list that represents the next system
            object_list = None

            if next_key in self.model_graphs:
                return None
            logging.info(f"Compiling system '{key_name}'")
            if next_key in self.item_refs:
                object_list = self.item_refs[next_key]
            elif len(next_key) > 1:
                self.handle_missing_subsystem(next_key, add_subsystems)
            else:
                object_list = self.handle_missing_topsystem(next_key)
                if object_list is None:
                    raise FatalCompilationError(f"Failed to compile model file for'{next_key[0]}'")

        if object_list is None:
            return None

        graph = self.object_list_to_graph(object_list, next_key, add_subsystems)
        self.model_graphs[next_key] = graph

    def fingerprint_to_part(self, fingerprint: ObjectFingerprint) -> Optional['MDLHandlerProtocol']:
        """

        :param fingerprint:
        :return:
        """
        if fingerprint in self.fingerprint_memo:
            return self.fingerprint_memo[fingerprint]
        matched = []
        for part in parts:
            if part.MDLHandler.object_filter(fingerprint):
                matched.append(part)
        if len(matched) < 1:
            report_error(f"An MDL object with this fingeprint '{fingerprint}' was not matched to any part.")
            return None
        elif len(matched) > 1:
            report_error(f"An MDL object with this fp '{fingerprint}' matched to more than one part."
                         + "\n\t".join([p.part_name for p in matched]))
            return None
        return matched[0].MDLHandler

    @staticmethod
    def handle_cds_params(graph: ModelGraph, obj: MDLObject) -> None:
        """
        Special handling of the cdsParameters object
        :param graph: ModelGraph to add the parameters to
        :param obj:
        :return:
        """
        obj_list = obj.value
        if not isinstance(obj_list, MDLObjectList):
            report_error("Parameter block was not a composite object in the MDL file")
            return
        for sub_obj in obj_list:
            if sub_obj.name == 'Name':
                graph.params = convert_parameter_list(str(sub_obj.value))
                return
        report_error("Got cdsParameters block without a name.")

    @staticmethod
    def type_merge_dicts(a: Dict[str, List], b: Dict[str, List]):
        """
        Merge two dictionaries so that items in lists of a are added to b
        only if the key doesn't exist in b or the type of the item doesn't exist in b
        :param a:
        :param b:
        :return:
        """
        merged_dict = b.copy()
        for key, value in a.items():
            if key in b:
                types_b = set([type(item) for item in b[key]])
                for v in value:
                    if key not in b or type(v) not in types_b:
                        merged_dict[key].append(v)
            else:
                merged_dict[key] = value

        return merged_dict

    def object_list_to_graph(self, obj_list: MDLObjectList, graph_key: GraphKey, add_subsystems: bool) -> ModelGraph:
        """

        :param obj_list:
        :param graph_key: needed to setup new keys for any subsystems encountered.
        :return:
        """
        subsys = "/".join(graph_key)
        with ErrorContext(f"'{subsys}'"):
            graph = ModelGraph()
            lines = []
            for obj in obj_list:
                # skip certain block types
                if obj.name in ["Annotation"]:
                    continue
                # short-circuit Line objects, collect them and handle them later
                if obj.name == 'Line':
                    lines.append(obj)
                    continue
                fp, sid = fingerprint_object(obj)
                if fp is None:
                    continue
                with ErrorContext(f"'{subsys}' SID={sid}"):
                    # handle the cdsParameters block specially
                    if fp.Tag == "cdsParameters":
                        self.handle_cds_params(graph, obj)
                    else:
                        part_mdl_handler = self.fingerprint_to_part(fp)
                        if part_mdl_handler is None:
                            continue
                        part_func = part_mdl_handler.object_converter
                        try:
                            defaults = self.get_default_block_params(graph_key[0], fp.BlockType)
                            if not isinstance(obj.value, MDLObjectList):
                                raise FatalCompilationError("Object type should be an object list")
                            obj_dict = obj.value.as_dict()
                            obj_dict = self.type_merge_dicts(defaults, obj_dict)
                            part = part_func(obj_dict)
                        except Exception as e:
                            report_error(f"Exception while converting object with fingerprint {fp}: {str(e)}")
                            raise e
                        graph.add_part(part)

                        # special subsystem handling.  Need to make graph for each subsystem
                        if isinstance(part, Subsystem):
                            if part.disposition == SubsystemDisposition.INTERNAL:
                                new_graph_key = graph_key + (part.name, )
                                if new_graph_key in self.item_refs:
                                    key = "/".join(new_graph_key)
                                    report_error(f"key '{key}' was parsed twice.  There may be a name collision.")
                                else:
                                    self.item_refs[new_graph_key] = part.code_blob
                            elif part.disposition == SubsystemDisposition.EXTERNAL:
                                new_graph_key = part.code_blob
                            else:
                                raise FatalCompilationError("Subsystem disposition was not one of the recognized values.")

                            part.graph_key = new_graph_key
                            key = "/".join(new_graph_key)
                            if(add_subsystems):
                                self.process_stack.append(ProcessStackItem(new_graph_key, True))

            # handle the lines last
            handle_lines(graph, lines)

        return graph

    def handle_missing_topsystem(self, next_key: GraphKey) -> Optional[MDLObjectList]:
        # it's a new file.  Parse it.
        logging.info(f"Looking for source file for {next_key[0]}")
        fname = next_key[0] + ".mdl"
        model_path = model_file_search(fname)
        if model_path is None:
            report_error(f"Could not find model file '{fname}' in RCG_LIB_PATH")
            return None
        logging.info(f"Found.  Parsing {model_path}.")
        top_list = parse_mdl_file(str(model_path))
        model_list = None
        for obj in top_list:
            if obj.name in ["Model", "Library"] and isinstance(obj.value, MDLObjectList):
                model_list = obj.value
                break
        if model_list is None:
            return None
        # find system sub-list
        system_obj = None
        for obj in model_list:
            if obj.name == "BlockParameterDefaults" and isinstance(obj.value, MDLObjectList):
                self.set_default_block_params(next_key[0], obj.value)
            if obj.name == "System" and isinstance(obj.value, MDLObjectList):
                system_obj = obj.value
        return system_obj

    def set_default_block_params(self, mdl_name: str, mdl_object_list: MDLObjectList) -> None:
        """
        Set default block parameters for the given mdl file
        :param mdl_name:
        :param mdl_object_list:
        :return:
        """
        for obj in mdl_object_list:
            if obj.name == "Block" and isinstance(obj.value, MDLObjectList):
                obj_dict = obj.value.as_dict()
                block_type = check_one_of_convertable(obj_dict, "BlockType", str)
                if mdl_name not in self.default_block_parameters:
                    self.default_block_parameters[mdl_name] = {}
                self.default_block_parameters[mdl_name][block_type] = obj_dict

    def get_default_block_params(self, mdl_name: str, block_type: str) -> MDLObjectListDictionary:
        """
        Return the default block parameters for a given mdl and block type
        :param mdl_name:
        :param block_type:
        :return:
        """
        return self.default_block_parameters.get(mdl_name, {}).get(block_type, {})

    def handle_missing_subsystem(self, next_key: GraphKey, add_subsystems: bool) -> None:
        """
        If a subsystem isn't cached yet, load the file
        :param next_key:
        :return:
        """
        root_key = (next_key[0],)
        if root_key not in self.item_refs:
            key = "/".join(next_key)
            self.process_stack.append(ProcessStackItem(next_key, add_subsystems))
            key = "/".join(root_key)
            self.process_stack.append(ProcessStackItem(root_key, False))
        else:
            # file was read already read, but this subsystem was not parsed
            subsystem_name = "/".join(next_key)
            report_error(f"Subsystem '{subsystem_name}' could not be found")
        return None

    def compile(self):
        while len(self.process_stack) > 0:
            self.compile_step()

    def compile_model(self) -> "Model":
        if not self.parts_compilable:
            raise FatalCompilationError("Not all RCG parts were properly defined for compiling MDL files.")

        self.compile()

        # get parameters

        # get DAQ names?

        # check for errors
        if error_count() > 0:
            raise FatalCompilationError("There were some errors during compilation")

        return Model(0,{}, [], self.model_graphs)