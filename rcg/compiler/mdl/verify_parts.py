from types import ModuleType

from ...parts import parts
from ...errors import report_error
from .mdl_handler_protocol import MDLHandlerProtocol


class WrongSignature(Exception):
    pass


def verify_mdl_functions(part_module: ModuleType) -> bool:
    """
    return true if a part has the necessary functions,
    otherwise return false and report error

    :param part_module: a part module
    :return: True if
    """
    if not hasattr(part_module, "part_name"):
        file_path = None
        if hasattr(part_module, "__file__"):
            file_path = part_module.__file__

        msg = """A part module does not have a part_name!
    Make sure that all part modules have 'part_name' defined in there __init__.py files"""
        if file_path is not None:
            msg = msg + f"\npath to the module is {file_path}"
        report_error(msg)
        return False
    part_name = part_module.part_name
    if type(part_module) is not ModuleType:
        report_error(f"part module '{part_name}' was not actually a module")
        return False
    if not hasattr(part_module, "MDLHandler") or not issubclass(part_module.MDLHandler, MDLHandlerProtocol):
        report_error(f"part module '{part_name}' needs a class named 'MDLHandler'" +
                     "\n\tThe 'MDLHandler' class should derive from 'MDLHandlerProtocol'"
                     )
        return False

    return True


def verify_parts() -> bool:
    """
    Verify all parts have the right mdl functions
    :return: True if each part in 'parts.parts' has the right mdl functions
    """
    good = True
    for part in parts:
        good = verify_mdl_functions(part) and good
    return good
