from typing import Protocol, runtime_checkable, TYPE_CHECKING, Dict, List, Optional

if TYPE_CHECKING:
    from .object_fingerprint import ObjectFingerprint
    from .item import MDLObject, MDLObjectListDictionary
    from ...parts.part import Part

@runtime_checkable
class MDLHandlerProtocol(Protocol):
    """
    Protocol for MDLHandler objects.  Every part should implement one
    and include it in the part module as "MDLHandler"
    """

    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if the MDL object represented by ObjectFingerprint should be turned into the part
        of which this MDLHandler is a member
        :param fingerprint:
        :return:
        """

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Convert the MDL object into a RCG part of the type of which thie MDLHandler is a member
        :return:
        """