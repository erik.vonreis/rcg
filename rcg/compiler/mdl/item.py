from typing import Callable, Union, List, Any, Iterable, Dict, TypeVar, TypeAlias

MDLNumber = Union["MDLInt", "MDLFloat", "MDLExp"]

MDLValue = Union["MDLString", MDLNumber]

MDLItem = Union["MDLObjectList", "MDLObject", MDLValue]

class MDLWord(object):
    """
    Represents a bare string as in an enumeration
    """
    def __init__(self, value: str):
        self.value = value

    def __str__(self):
        return self.value

    def __repr__(self):
        return f'MDLWord("{self.value}")'

    def __eq__(self, other: "MDLWord"):
        return self.value == other.value


# type of an MDLObjectList converted to a dictionary
MDLObjectListDictionary = Dict[str, List[MDLItem]]


class MDLObjectList(list):
    """

    Represents the lists of objects in a compound value
    """

    def indent_print(self, indent: int) -> str:
        txt = "".join([x.indent_print(indent) for x in self])
        return txt

    def __str__(self) -> str:
        return self.indent_print(0)

    def find(self, predicate: Callable[["MDLObject"], bool]) -> List["MDLObject"]:
        """
        Return any object, or subobject from_part the list that matches the predicate
        :param predicate: A callable that takes a MDLObject. It returns True of the object matches the predicate
        :return: A list of objects, including recursively any sub objects that match the predicate
        """
        results = [obj.find(predicate) for obj in self]
        matches = []
        for m in results:
            matches += m
        return matches

    def as_dict(self) -> MDLObjectListDictionary:
        """
        Return a dictionary of value lists if there are multiples, or a single value.

        the key is the object name, and the members of the associated
        list are all objects in self that have that name.
        :return:
        """
        dict: Dict[str, List[MDLItem]] = {}
        for obj in self:
            if obj.name in dict:
                # turn into list
                dict[obj.name].append(obj.value)
            else:
                dict[obj.name] = [obj.value]
        return dict


T = TypeVar('T')
def identity(x: T) -> T: return x


S = TypeVar('S')


def check_one_of(obj_dict: Dict[str, List[MDLItem]], key: str, mdl_type: type, convert_type: Callable[...,S], default: S|None = None) -> S:
    """
    Check that there's exactly one item of a certain mdl_type at the given key.  Any value with the name not of the
    type causes an error.

    If it exists and there's only one convert the object to the convert_type and return it.

    Otherwise throw an Exception
    :param obj_dict:
    :param key:
    :param mdl_type:
    :param convert_type:
    :return:
    """
    if key not in obj_dict:
        if default is None:
            raise Exception(f"'{key}' was not found in the MDLObjectList.")
        else:
            return default
    val_list = obj_dict[key]
    if len(val_list) == 0:
        if default is None:
            raise Exception(f"'{key}' was found in the MDLObjectList but no value.  This is a bug in the compiler.")
        else:
            return default
    # value should not be empty
    if len(val_list) > 1:
        raise Exception(f"More than one value with '{key}' was found in the MDLObjectList, but exactly one is required.")
    obj = val_list[0]
    if not isinstance(obj, mdl_type):
        raise Exception(f"Value of '{key}' was expected to be of type {mdl_type} but was of type {type(obj)}")
    return convert_type(obj)


def check_one_of_type(obj_dict: Dict[str, List[MDLItem]], key: str, mdl_type: type, convert_type: Callable[...,S], default: [S | None] = None) -> S:
    """
    Check that there's exactly one item of a certain mdl_type at the given key.  Ignores any nof that type.

    If it exists and there's only one convert the object to the convert_type and return it.

    Otherwise throw an Exception
    :param obj_dict:
    :param key:
    :param mdl_type:
    :param convert_type:
    :return:
    """
    if key not in obj_dict:
        if default is None:
            raise Exception(f"'{key}' was not found in the MDLObjectList.")
        else:
            return default
    val_list = [x for x in obj_dict[key] if isinstance(x, mdl_type)]
    if len(val_list) == 0:
        if default is None:
            raise Exception(f"'{key}' was found in the MDLObjectList but no value.  This is a bug in the compiler.")
        else:
            return default
    # value should not be empty
    if len(val_list) > 1:
        raise Exception(f"More than one value with '{key}' was found in the MDLObjectList, but exactly one is required.")
    obj = val_list[0]
    return convert_type(obj)


def check_one_of_convertable(obj_dict: Dict[str, List[MDLItem]], key: str, convert_type: Callable[...,S], default: S | None = None) -> S:
    """
    Check that there's exactly one item that is convertable to the convert type.  Any value with the name that can't
    convert causes an error

    If it exists and there's only one convert the object to the convert_type and return it.

    Otherwise throw an Exception
    :param obj_dict:
    :param key:
    :param convert_type:
    :return:
    """
    if key not in obj_dict:
        if default is None:
            raise Exception(f"'{key}' was not found in the MDLObjectList.")
        else:
            return default
    val_list = obj_dict[key]
    if len(val_list) == 0:
        if default is None:
            raise Exception(f"'{key}' was found in the MDLObjectList but no value.  This is a bug in the compiler.")
        else:
            return default
    # value should not be empty
    if len(val_list) > 1:
        raise Exception(f"More than one value with '{key}' was found in the MDLObjectList, but exactly one is required.")
    obj = val_list[0]
    return convert_type(obj)


class MDLInt(object):
    """Represents an integer."""
    def __init__(self, token: str):
        """
        :param token:  a string.  the text of the number token
        """
        self.text = token
        self.value = int(token)

    def __str__(self) -> str:
        return str(self.value)

    def __repr__(self) -> str:
        return f'MDLInt("{self}")'

    def __eq__(self, other: MDLNumber) -> bool:
        return self.value == other.value

    def __int__(self):
        return self.value

    def __float__(self):
        return float(self.value)


class MDLFloat(object):
    """Represents a floating point number to be written in decimal format."""

    def __init__(self, token: str):
        """
        :param token:  a string.  the text of the number token
        """
        self.text = token
        self.value = float(token)
        self.precision = len(token) - token.find('.') - 1

    def __str__(self) -> str:
        return f"%.{self.precision}f" % self.value

    def __float__(self) -> float:
        return self.value

    def __repr__(self) -> str:
        return f'MDLFloat("{self}")'

    def __eq__(self, other: MDLNumber) -> bool:
        return self.value == other.value


class MDLExp(object):
    """Represents a floating point number to be written in exponential format."""

    def __init__(self, token: str):
        """
        :param token:  a string.  the text of the number token
        """
        self.text = token
        self.value = float(token)
        eloc = token.find("e")
        if eloc < 0:
            eloc = token.find("E")
        dot_loc = token.find('.')
        if dot_loc < 0:
            self.precision = 0
        else:
            self.precision = eloc - dot_loc - 1

    def __str__(self) -> str:
        return f"%.{self.precision}e" % self.value

    def __repr__(self) -> str:
        return f'MDLExp("{self}")'

    def __eq__(self, other: MDLNumber) -> bool:
        return self.value == other.value

    def __float__(self):
        return self.value

class MDLString(object):
    """
    Represents an mdl file_name string which can be several actual strings concatenated together.
    This class preserves the concatenation
    """
    def __init__(self, s: str = None):
        self.value = s or ""

    def __add__(self, other: Union["MDLString", str]) -> "MDLString":
        if isinstance(other, MDLString):
            r = MDLString(self.value + other.value)
            return r
        elif isinstance(other, str):
            r = MDLString(self.value + other)
            return r
        raise TypeError("Expected MDLString but got " + str(type(other)))

    def indented_print(self, indent: int, start_col: int) -> str:
        first_line_end = 119
        second_line_end = 120

        s = self.value

        #first line
        max_first_line = first_line_end - start_col - 2 # accounting for two double quotes
        if len(s) <= max_first_line:
            return f'"{s}"'

        backslash_count = 0
        ptr = max_first_line - 1
        while s[ptr] == '\\':
            ptr -= 1
            backslash_count += 1
        if backslash_count % 2 > 0:
            size = max_first_line + 1
        else:
            size = max_first_line
        lines = [ f'"{s[:size]}"' ]
        s = s[size:]

        indent_txt = indent_to_ws(indent)
        max_second_line = second_line_end - len(indent_txt) - 2

        while len(s) > max_second_line:
            backslash_count = 0
            ptr = max_second_line - 1
            while s[ptr] == '\\':
                ptr -= 1
                backslash_count += 1
            if backslash_count % 2 > 0:
                size = max_second_line + 1
            else:
                size = max_second_line
            lines.append( f'"{s[:size]}"' )
            s = s[size:]

        if len(s) > 0:
            lines.append(f'"{s}"')

        return f"\n{indent_txt}".join(lines)

    def __repr__(self) -> str:
        maxlen = 10
        if len(self.value) == 0:
            return 'MDLString("")'
        if len(self.value)  <= maxlen:
            return f'MDLString("{self.value}")'
        return f'MDLString("{self.value[:maxlen]}...")'

    def __str__(self) -> str:
        return self.value


    def __eq__(self, other):
        return str(self) == str(other)

    def __float__(self):
        return float(self.value)

    def __int__(self):
        return int(self.value)


def print_array_value(a: Iterable[Any]) -> str:
    if len(a) > 0 and isinstance(a[0], list):
        # is nested list.
        sublists = [", ".join([str(i) for i in b]) for b in a]
        return "[" + "; ".join(sublists) + "]"
    else:
        return '[' + ", ".join([str(i) for i in a]) + ']'


def print_simple_value(val: Any) -> str:

    if isinstance(val, list):
        return print_array_value(val)
    return str(val)


def indent_to_ws(indent, start=0) -> str:
    """
    Return a string that will advance from_part starting column to the indent position.

    Returns a single space if start is on or after indent.

    Assumes size 8 tabs.  Use a single tab to get to a column that's multiple of  8,
    then uses tabs until less than 8 columns are left
    columns are to go, then uses spaces again.  This is to match behavior Matlab mdl files.

    :param indent: Number of columns to indent from_part the left of the screen.
    :param start: Columns to start padding at.
    :return: string that will pad the line out from_part column start for a total of indent columns from_part the left of the screen
    """

    #special case of no indent, just return empty string
    if indent == 0:
        return ""

    # if we've already gone past the mark, just skip one column
    if start >= indent:
        indent = start + 1

    to_pad = indent - start

    # get the number of spaces until the next multiple of 8 column
    leading_spaces = (8 - (start % 8)) % 8

    # won't even reach multiple of 8
    if leading_spaces > to_pad:
        return " " * to_pad

    if leading_spaces > 0:
        txt = "\t"
    else:
        txt = ""

    to_pad -= leading_spaces

    txt += "\t" * (to_pad // 8) + " " * (to_pad % 8)

    return txt


def print_value(val: Any, indent: int, line_length: int, line_length_chars: int) -> str:
    """
    create a string representation of an object value as it ought to look in an mdl file_name.

    :param val: the value to print
    :param indent: How many columns to the right this object is indented
    :param line_length: line length so far in columns
    :param line_length_chars: line length so far in chars (needed for string printing)
    :return: returns the string containing value as txt suitable for mdl file_name
    """
    if isinstance(val, MDLObjectList):
        return " {\n" + val.indent_print(indent+2) + indent_to_ws(indent) + "}"

    # try to start on column 25, adjusted for indent
    txt = indent_to_ws(24+indent, line_length)

    if isinstance(val, MDLString):
        txt += val.indented_print(indent, line_length_chars + len(txt))
    else:
        txt += print_simple_value(val)
    return txt


class MDLObject(object):
    """
    Represents a single MDL object
    """
    def __init__(self, name: str, value: MDLItem):
        self.name = name
        self.value = value

    def __str__(self) -> str:
        return self.print()

    def print(self) -> str:
        return self.indent_print(0)

    def indent_print(self, indent: int) -> str:
        txt = indent_to_ws(indent)
        txt += self.name

        txt += print_value(self.value, indent, indent + len(self.name), len(txt))
        txt += "\n"
        return txt

    def __repr__(self) -> str:
        return f"MDLObject({self.name}, ...)"

    def __eq__(self, other: "MDLObject") -> bool:
        return self.name == other.name and self.value == other.value

    def find(self, predicate: Callable[["MDLObject"], bool]) -> List["MDLObject"]:
        """
        Return a list of objects that cause predicate to return true when passed as an argument.
        The list may include this object, or recursively any subobjects.  If predicate(self) is true,
        then self is always the first object on the returned list.
        :param predicate:
        :return:
        """
        if predicate(self):
            ret = [self]
        else:
            ret = []

        if isinstance(self.value, MDLObjectList):
            ret += self.value.find(predicate)

        return ret
