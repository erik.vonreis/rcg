from dataclasses import dataclass, fields
from typing import Dict, Callable, Optional, Tuple
from .item import MDLObject, MDLObjectList


@dataclass(frozen=True)
class ObjectFingerprint:
    name: str
    BlockType: str
    Tag: str
    SourceBlock: str

fingerprint_memo: Dict[ObjectFingerprint, Callable]

def fingerprint_object(obj: MDLObject) -> Tuple[Optional[ObjectFingerprint], Optional[int]]:
    sid = None
    if not isinstance(obj.value, MDLObjectList):
        return None, None
    fields_dict = {f.name: f for f in fields(ObjectFingerprint)}
    vals: Dict[str, Optional[str]] = {f: None for f in fields_dict.keys()}
    for subobj in obj.value:
        if subobj.name in vals:
            vals[subobj.name] = fields_dict[subobj.name].type (subobj.value)
        if subobj.name == 'SID':
            sid = int(str(subobj.value))
    vals["name"] = obj.name
    return ObjectFingerprint(**vals), sid
