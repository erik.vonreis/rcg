from pathlib import Path
from typing import List, Optional
import os

from ..errors import report_warning, report_error

RCG_LIB_PATH: Optional[List[Path]] = None


def model_file_search(model_file: str, short_circuit_search=False, rcg_lib_path: Optional[List[Path]] = None) -> Optional[Path]:
    """
    Return the absolute path to a model file_name found by searching rcg_lib_path if it is not None, or RCG_LIB_PATH
    environment variable otherwise.

    Directories are searched in order.

    if short_circuit is true, return immediately when the file_name is found.

    If short circuit is false, continue checking all directories.  Report a warning if the file_name is found more than once.

    :param model_file: name of model file_name
    :param short_circuit_search: If True,
    :param rcg_lib_path:
    :return: Returns the first such file_name found, or None if nothing is found.
    """
    global RCG_LIB_PATH

    # determine with search paths
    if rcg_lib_path is None:
        if RCG_LIB_PATH is None:
            try:
                RCG_LIB_PATH = [Path(d) for d in os.environ["RCG_LIB_PATH"].split(':')]
            except Exception as e:
                report_error(f"""Invalid value set to RCG_LIB_PATH environment variable: {str(e)}.
    The value should be a ':' separated list of paths to search for model files.""")
                return None
        search_path = RCG_LIB_PATH
    else:
        search_path = rcg_lib_path

    return file_search(model_file, short_circuit_search, search_path)


def file_search(file_name: str, short_circuit_search=False, search_path: List[Path] = None ) -> Optional[Path]:
    """
    Return the absolute path to a file found by searching rcg_lib_path if it is not None, or RCG_LIB_PATH
    environment variable otherwise.

    Directories are searched in order.

    if short_circuit is true, return immediately when the file is found.

    If short circuit is false, continue checking all directories.  Report a warning if the file is found more than once.

    :param file_name: name of model file
    :param short_circuit_search: If True,
    :param search_path: A list of directories to search in
    :return: Returns the first such file found, or None if nothing is found.
    """
    global RCG_LIB_PATH

    # determine with search paths
    found: Optional[Path] = None

    for search_dir in search_path:
        path = Path(search_dir, file_name)
        if path.is_file():
            if found is None:
                found = path
            else:
                report_warning(f"""model file_name {file_name} was found more than once in the RCG lib path
    first here: {found}
    later here: {path}
    the first file_name will be used.""")
            if short_circuit_search:
                break
    return found

