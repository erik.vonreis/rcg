from typing import Protocol, TYPE_CHECKING, Dict

from ..errors import report_warning

if TYPE_CHECKING:
    from ..model.model import Model


class FatalCompilationError(Exception):
    pass

class Compiler(Protocol):
    """
    Interface to a model compiler.

    A compiler takes source code and produces a collection of abstract graphs of parts.

    Each level of nesting of a graph is a new graph.
    """
    def __init__(self, model_name: str): None

    def compile_model(self) -> "Model": None


# some utility functions

def str_converter(s: str) -> str | int | float:
    """
    Convert a string to an int, or failing that a float, or failing that just return a string
    :param s:
    :return:
    """
    value: str | int | float = s

    try:
        value = int(value)
    except ValueError:
        try:
            value = float(value)
        except ValueError:
            pass

    return value


Parameters = Dict[str, str | int | float]


def convert_parameter_list(param_block: str, error_tag: str | None = None) ->Parameters:
    """
    Convert a parameter list, such as for an ADC block, into a dictionary.

    param lists are given as single string.  They are broken down by newlines.  Any commas before the newline are dropped
    the resulting string is stripped of leading and trailing white space.
    the string is split on the first '=' character.  If this isn't found, a warning is issues and no parameter is
    parsed.
    Otherwise, the left side is stripped and saved as the key.
    The right side is stripped and saved as an integer if possible, or failing that a float, or failing that a string.
    :param param_block:
    :param error_tag: optional string that will be printed to help identify source of problem in warnings
    :return:
    """
    param_dict: Parameters = {}

    param_list = param_block.split('\n')

    for param_text_raw in param_list:
        param_text = param_text_raw.strip(",\r \t")
        if len(param_text) < 1:
            # skip as empty line
            continue
        param_items = param_text.split('=', 1)
        if len(param_items) < 2:
            # warn: bad parameter can't parse
            msg = f"parameter {param_text} could not be parsed"
            if error_tag is not None:
                msg += f" in '{error_tag}'"
            report_warning(msg)
        else:
            key = param_items[0].strip()

            value = str_converter(param_items[1].strip())

            param_dict[key] = value

    return param_dict


