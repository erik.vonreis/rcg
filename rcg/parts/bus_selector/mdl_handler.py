from typing import TYPE_CHECKING
from .bus_selector import BusSelector
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, MDLObjectListDictionary, check_one_of, MDLString
from ...compiler.compiler import FatalCompilationError, convert_parameter_list

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fingerprint.name == 'Block' and fingerprint.BlockType == 'BusSelector':
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        try:
            name = check_one_of(obj_dict, 'Name', MDLString, str)
            output_signals_str = check_one_of(obj_dict, 'OutputSignals', MDLString, str)
        except Exception as e:
            raise FatalCompilationError(f"A Bus Selector failed to compile: {str(e)}")

        output_signals = output_signals_str.split(',')

        return BusSelector(name, output_signals)

# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
