from typing import List, Dict

from ..part import Part
from ..data_types import Bus, AnyType
from ..port import Port


class BusSelector(Part):
    def __init__(self, name: str, output_signals: List[str]):
        """

        :param name:
        :param output_signals:
        """
        self.name = name
        self.output_signals = output_signals

        self.num_channels = len(self.output_signals)

        # create bus types
        any = AnyType()
        bus = Bus({s: any for s in output_signals})
        self.in_ports = [Port(bus)]
        self.out_ports = [Port(any)] * self.num_channels

        self.calculable = False
