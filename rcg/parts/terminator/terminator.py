from ..part import Part
from ..data_types import AnyType
from ..port import Port


class Terminator(Part):
    def __init__(self, name: str):
        """

        :param name:
        """
        self.name = name
        self.in_ports = [Port(AnyType)]
        self.out_ports = []

