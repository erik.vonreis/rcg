from ..part import Part
from ..data_types import Float64
from ..port import Port


class TestPoint(Part):
    def __init__(self, name: str):
        """

        :param name:
        """
        self.name = name
        self.in_ports = [Port(Float64())]
        self.out_ports = []

