from ..part import Part
from ..data_types import Float64
from ..port import Port


class Switch(Part):
    def __init__(self, name: str):
        """

        :param name:
        """
        self.name = name

        self.out_ports = [Port(Float64())]
        self.in_ports = [Port(Float64())] * 3

