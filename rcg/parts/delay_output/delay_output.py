from typing import TYPE_CHECKING

from ..port import Port
from ..data_types import AnyType
from ..part import Part

if TYPE_CHECKING:
    from ..delay.delay import Delay


class DelayOutput(Part):
    """Sister part that's automatically generated when a Delay part is compiled"""
    def __init__(self, name: str, delay_input: 'Delay'):
        self.name = name
        self.in_ports = []
        self.out_ports = [Port(AnyType())]
        self.delay_input = delay_input
