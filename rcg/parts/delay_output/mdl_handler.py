from typing import TYPE_CHECKING
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.compiler import FatalCompilationError

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ...compiler.mdl.item import MDLObjectListDictionary
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        raise FatalCompilationError("DelayOutput object should never be directly created by the compiler")


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
