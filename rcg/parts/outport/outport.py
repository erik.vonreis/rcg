from ..part import Part
from ..data_types import AnyType
from ..port import Port


class Outport(Part):
    """
    Represent an internal output port in a subsystem
    """
    def __init__(self, name: str, port_num: int):
        """

        :param name:
        """
        self.name = name
        self.port_num = port_num
        self.in_ports = [Port(AnyType())]
        self.out_ports = [Port(AnyType())]  # outport needed for merging with parent system

        self.calculable = False
