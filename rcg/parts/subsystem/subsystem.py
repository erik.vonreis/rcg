from typing import TYPE_CHECKING, Optional, Any, Set
from enum import Enum
from ..part import Part
from ..data_types import AnyType
from ..port import Port

if TYPE_CHECKING:
    from ...model.model_graph import GraphKey


class SubsystemDisposition(Enum):
    INTERNAL=0
    EXTERNAL=1


class Subsystem(Part):
    """Can represent either an internal subsystem or a reference to an external subsystem"""
    def __init__(self, name: str, num_in_ports: int, num_out_ports: int, disposition: SubsystemDisposition, code_blob: Any, tags: Set[str]):
        """

        :param name:
        :param num_in_ports: number of input ports
        :param num_out_ports: number of output ports
        :param disposition: Whether subsystem is internal or external
        :param code_blob: Use this to pass the code for the internals of a subsystem back to the compiler
        """
        self.name = name
        self.in_ports = [Port(x) for x in [AnyType] * num_in_ports]
        self.out_ports = [Port(x) for x in [AnyType] * num_out_ports]
        self.graph_key: Optional[GraphKey] = None
        self.code_blob = code_blob
        self.disposition = disposition
        self.tags = tags


        