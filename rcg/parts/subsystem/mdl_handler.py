from typing import TYPE_CHECKING
from .subsystem import Subsystem, SubsystemDisposition
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, MDLObjectList, check_one_of, identity, MDLString
from ...compiler.compiler import FatalCompilationError
from ..mdl_helpers import get_mdl_object_ports

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part
    from ...model import GraphKey

def check_cds_starts(source_block: str) -> bool:
    """
    Returns true if the source_block starts with any of the cds part identifiers
    :param source_block:
    :return:
    """
    ids = ['cds', 'dac/']

    for id in ids:
        if source_block.startswith(id):
            return True
    return False


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fp: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fp:
        :return:
        """
        if fp.name == 'Block' and fp.BlockType == 'SubSystem' and fp.SourceBlock is None:
            return True
        if fp.name == 'Block' and fp.BlockType == 'Reference' and fp.SourceBlock is not None and not check_cds_starts(fp.SourceBlock) :
            return True
        return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        

        try:
            tags = check_one_of(obj_dict, 'Tag', MDLString, str, "")
            name = check_one_of(obj_dict, 'Name', MDLString, str)
        except Exception as e:
            raise FatalCompilationError(f"A Subsystem part failed to compile: {str(e)}")

        ports = get_mdl_object_ports(obj_dict, "Substring")

        source_block = obj_dict.get('SourceBlock', None)
        if source_block is None:
            try:
                code_blob = check_one_of(obj_dict, 'System', MDLObjectList, identity)
            except Exception as e:
                raise FatalCompilationError(f"MDLObject named {mdl_object.name} didn't have a good system subblock: {str(e)}")
            disposition = SubsystemDisposition.INTERNAL
        else:
            disposition = SubsystemDisposition.EXTERNAL
            code_blob: GraphKey = tuple(str(source_block[0]).split('/'))

        tag_list = set([s.strip() for s in tags.split(" \n\r\t,")])

        return Subsystem(name, ports[0], ports[1], disposition, code_blob, tag_list)


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
