from ..part import Part
from ..data_types import Float64, ArbitraryLengthVector
from ..port import Port


class FiltMuxMatrix(Part):
    """
    Matrix multiplication that takes mux input and gives output
    """
    def __init__(self, name: str):
        """

        :param name:
        """
        self.name = name

        self.out_ports = [Port(ArbitraryLengthVector(Float64()))]
        self.in_ports = [Port(ArbitraryLengthVector(Float64()))]

