from ..part import Part
from ..data_types import AnyType
from ..port import Port


class From(Part):
    """
    Represent a  from tag
    """
    def __init__(self, name: str, goto_tag: str):
        """

        :param name:
        """
        self.name = name

        self.out_ports = [Port(AnyType())]

        # Used to connect to Goto part
        self.in_ports = [Port(AnyType())]

        self.goto_tag = goto_tag

        self.calculable = False