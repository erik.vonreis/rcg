from typing import Dict

from ..data_types import Float64
from ..port import Port
from ..part import Part


class Osc(Part):
    def __init__(self, name: str, params: Dict[str, str]):
        """
        :param name:
        """
        self.name = name
        self.params = params
        self.out_ports = [Port(Float64())] * 3
        self.in_ports = [Port(Float64())]

