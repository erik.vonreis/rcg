from ..part import Part
from ..data_types import AnyType
from ..port import Port


class Inport(Part):
    """
    Represent a  internal input port on a subsystem
    """
    def __init__(self, name: str, port_num: int):
        """

        :param name:
        """
        self.name = name
        self.port_num = port_num
        self.in_ports = [Port(AnyType())]  # inport needed for merging into parent system
        self.out_ports = [Port(AnyType())]

        self.calculable = False

