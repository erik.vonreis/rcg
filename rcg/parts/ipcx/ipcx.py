from enum import Enum
from ..part import Part
from ..data_types import Float64
from ..port import Port
from ...errors import report_error


class IpcType(Enum):
    SHMEM = 0
    RFM = 1
    PCIE = 2

class IPCx(Part):
    """
    Square root
    """
    def __init__(self, name: str, ipc_type: IpcType):
        """

        :param name:
        """
        self.name = name
        self.ipc_type = ipc_type

        self.in_ports = [Port(Float64())]
        self.out_ports = [Port(Float64())] * 2

