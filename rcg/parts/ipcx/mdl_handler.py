from typing import TYPE_CHECKING
from .ipcx import IPCx, IpcType
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, MDLObjectList, check_one_of, MDLString, check_one_of_type
from ...compiler.compiler import FatalCompilationError
from ..mdl_helpers import get_mdl_object_name

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fp: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fp.name == 'Block' and fp.Tag is not None and fp.Tag.startswith('cdsIPCx'):
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        
        try:
            name = check_one_of(obj_dict, 'Name', MDLString, str)
            tag = check_one_of_type(obj_dict, 'Tag', MDLString, str)
        except Exception as e:
            raise FatalCompilationError(
                f"A Subsystem Sum failed to compile: {str(e)}")

        tag_split = tag.split("_", 1)
        if len(tag_split) != 2:
            raise FatalCompilationError(f"Type not found in IPC tag '{tag}'")
        ipc_type = IpcType[tag_split[1]]

        return IPCx(name, ipc_type)


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
