from ..data_types import Float64
from ..port import Port
from ..part import Part


class Atan2(Part):
    def __init__(self, name: str):
        """
        :param name:
        """
        self.name = name
        self.in_ports = [Port(Float64())] * 2
        self.out_ports = [Port(Float64())]

