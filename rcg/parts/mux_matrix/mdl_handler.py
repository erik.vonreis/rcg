from typing import TYPE_CHECKING
from .mux_matrix import MuxMatrix
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary
from ..mdl_helpers import get_mdl_object_name

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fingerprint.name == 'Block' and fingerprint.Tag == 'cdsMuxMatrix':
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        
        name = get_mdl_object_name(obj_dict, "MuxMatrix")
        return MuxMatrix(name)


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
