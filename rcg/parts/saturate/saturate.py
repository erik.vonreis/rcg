from ..part import Part
from ..data_types import Float64
from ..port import Port
from ...errors import report_error


class Saturate(Part):
    """
    Saturate a value so that it maxes out at the upper and lower limits.
    """
    def __init__(self, name: str, lower_limit: float, upper_limit: float):
        """

        :param name:
        """
        self.name = name
        self.upper_limit = upper_limit
        self.lower_limit = lower_limit

        self.out_ports = [Port(Float64())]
        self.in_ports = [Port(Float64())]

