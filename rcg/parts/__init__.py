import sys
from importlib import import_module

# parts that have some special graph handling
from .goto import Goto
from .from_part import From
from .delay import Delay
from .subsystem import Subsystem, SubsystemDisposition
from .inport import Inport
from .outport import Outport

# list of parts to import
module_names_str = "goto from_part delay subsystem ground adc bus_selector dac inport outport filter epics_in"
module_names_str += " epics_out terminator sum product abs constant switch mux demux sqrt bitwise ezca_write filt_ctrl"
module_names_str += " epics_bin_in saturate mux_matrix contec_1616_dio test_point epics_momentary excitation fcn"
module_names_str += " ipcx unwrap_phase osc_fixed_phase osc math filt_mux_matrix ramp_mux_matrix logical_operator"
module_names_str += " epics_out_long epics_in_ctrl epics_string_in atan2"
module_names = module_names_str.split()

parts = [import_module("." + m, package=__name__) for m in module_names]
