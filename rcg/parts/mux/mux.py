from ..part import Part
from ..data_types import Vector, AnyType
from ..port import Port


class Mux(Part):
    def __init__(self, name: str, num_channels: int):
        """

        :param name:
        :param num_channels:
        """
        self.name = name

        self.num_channels = num_channels

        # create bus types
        any = AnyType()
        vec = Vector([any] * self.num_channels)
        self.out_ports = [Port(vec)]
        self.in_ports = [Port(any)] * self.num_channels

