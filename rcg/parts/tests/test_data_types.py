import unittest
from rcg.parts.data_types import AnyType, Float64, Bus, Vector, ArbitraryLengthVector


class TestDataTypes(unittest.TestCase):
    def setUp(self):
        self.f64_a = Float64()
        self.f64_b = Float64()


        self.bus_a = Bus({'chan1': Float64(), 'chan2': Float64(), 'chan3': Float64()})
        self.bus_b = Bus({'chan1': Float64(), 'chan2': Float64()})
        self.bus_c = Bus({'chan1': Float64(), 'chan2': Float64(), 'chan3': self.bus_b})
        self.bus_d = Bus({'chan1': AnyType(), 'chan3': AnyType()})

        self.vec_a = Vector([Float64(), Float64(), Float64()])
        self.vec_b = Vector.homogeneous_vector_of_scalars(3)
        self.vec_c = Vector.homogeneous_vector(4, self.vec_b)
        self.vec_d = Vector([Float64(), Float64(), AnyType()])
        self.vec_e = Vector([Float64(), AnyType()])
        self.vec_f = Vector([Float64(), self.bus_c, self.vec_a])
        self.vec_g = Vector([self.vec_e, self.vec_e])

        self.avec_a = ArbitraryLengthVector(Float64())
        self.avec_b = ArbitraryLengthVector(self.vec_e)
        self.avec_c = ArbitraryLengthVector(AnyType())

    def test_scalar(self):
        self.assertEqual(self.f64_a, self.f64_b)
        self.assertNotEqual(self.f64_a, self.bus_a)
        self.assertNotEqual(self.f64_a, AnyType())

        self.assertTrue(self.f64_a.can_convert_from(self.f64_b))
        self.assertFalse(self.f64_a.can_convert_from(AnyType()))

    def test_any(self):
        self.assertEqual(AnyType(), AnyType())
        self.assertTrue(AnyType().can_convert_from(AnyType()))
        self.assertTrue(AnyType().can_convert_from(self.f64_a))
        self.assertTrue(AnyType().can_convert_from(self.bus_a))
        self.assertTrue(AnyType().can_convert_from(self.vec_a))

    def test_vector_homogeneity_detection(self):
        self.assertTrue(self.vec_a.homogeneous)
        self.assertEqual(self.vec_a.shape, (3, ))

        self.assertFalse(self.vec_d.homogeneous)
        self.assertIsNone(self.vec_d.shape)

        self.assertTrue(self.vec_b.homogeneous)

        self.assertTrue(self.vec_c.homogeneous)
        self.assertEqual(self.vec_c.shape, (4, 3))

    def test_bus_convert(self):
        self.assertFalse(self.bus_a.can_convert_from(self.vec_a))
        self.assertFalse(self.bus_a.can_convert_from(self.bus_b))
        self.assertTrue(self.bus_a.can_convert_from(self.bus_a))
        self.assertTrue(self.bus_b.can_convert_from(self.bus_a))
        self.assertFalse(self.bus_a.can_convert_from(self.bus_c))
        self.assertFalse(self.bus_c.can_convert_from(self.bus_a))

        self.assertTrue(self.bus_d.can_convert_from(self.bus_a))
        self.assertFalse(self.bus_d.can_convert_from(self.bus_b))
        self.assertTrue(self.bus_d.can_convert_from(self.bus_c))


    def test_vector_convert(self):
        self.assertTrue(self.vec_a.can_convert_from(self.vec_a))
        self.assertTrue(self.vec_a.can_convert_from(self.vec_b))
        self.assertTrue(self.vec_b.can_convert_from(self.vec_a))

        self.assertTrue(self.vec_d.can_convert_from(self.vec_a))
        self.assertFalse(self.vec_e.can_convert_from(self.vec_a))
        self.assertFalse(self.vec_a.can_convert_from(self.vec_d))

        self.assertTrue(self.vec_c.can_convert_from(self.vec_c))


    def test_bus_equal(self):
        self.assertNotEqual(self.bus_a, self.bus_b)
        self.assertNotEqual(self.bus_a, self.bus_c)
        self.assertNotEqual(self.bus_a, self.bus_d)

        self.assertEqual(self.bus_c, self.bus_c)

    def test_vector_equal(self):
        self.assertEqual(self.vec_a, self.vec_b)
        self.assertEqual(self.vec_c, self.vec_c)
        self.assertNotEqual(self.vec_d, self.vec_a)
        self.assertNotEqual(self.vec_d, self.vec_f)
        self.assertEqual(self.vec_f, self.vec_f)

    def test_vector_shape(self):
        self.assertFalse(self.vec_g.homogeneous)
        self.assertIsNone(self.vec_g.shape)

    def test_arbitrary_vectors(self):
        self.assertTrue(self.avec_a.homogeneous)
        self.assertFalse(self.avec_b.homogeneous)
        self.assertTrue(self.avec_a.can_convert_from(self.vec_a))
        self.assertTrue(self.avec_c.can_convert_from(self.avec_a))
        self.assertTrue(self.avec_c.can_convert_from(self.vec_a))
        self.assertTrue(self.avec_c.can_convert_from(self.avec_b))

if __name__ == "__main__":
    unittest.main()