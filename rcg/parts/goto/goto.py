from ..data_types import AnyType
from ..port import Port
from ..part import Part


class Goto(Part):
    def __init__(self, name: str, goto_tag: str):
        """
        :param name:
        """
        self.name = name
        self.in_ports = [Port(AnyType())]

        # used to connect to From part.
        self.out_ports = [Port(AnyType())]

        self.goto_tag = goto_tag

        self.calculable = False