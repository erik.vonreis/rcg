from typing import TYPE_CHECKING
from .goto import Goto
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, check_one_of, MDLString
from ...compiler.compiler import FatalCompilationError


if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fingerprint.name == 'Block' and fingerprint.BlockType == 'Goto':
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        
        try:
            name = check_one_of(obj_dict, 'Name', MDLString, str)
            goto_tag = check_one_of(obj_dict, 'GotoTag', MDLString, str)
        except Exception as e:
            raise FatalCompilationError(f"A Filter failed to compile: {str(e)}")

        return Goto(name, goto_tag)


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
