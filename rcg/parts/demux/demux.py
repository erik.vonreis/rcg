from typing import List, Dict

from ..part import Part
from ..data_types import Vector, AnyType
from ..port import Port


class Demux(Part):
    def __init__(self, name: str, num_channels: int):
        """

        :param name:
        :param num_channels:
        """
        self.name = name

        self.num_channels = num_channels

        # create bus types
        any = AnyType()
        vec = Vector([any] * self.num_channels)
        self.in_ports = [Port(vec)]
        self.out_ports = [Port(any)] * self.num_channels
