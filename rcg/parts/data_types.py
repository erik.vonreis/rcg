from typing import Optional, Dict, List, Protocol
from ..exceptions import DuplicateNameError, InvalidDataType


class DataType(Protocol):
    """
    Interface to all DataType objects
    """

    scalar = True
    homogeneous = False
    shape = None

    def can_convert_from(self, other_type: "DataType") -> bool:
        """
        Return true if an input of this type can be read from_part an output of other_type
        :param other_type:
        :return:
        """

    def __eq__(self, other: "DataType") -> bool:
        """
        Return true if the other type is equivalent ot self.
        :param other:
        :return:
        """


class AnyType(DataType):

    scalar = True

    def can_convert_from(self, other_type: DataType) -> bool:
        """
        The AnyType type can be converted from_part any other type
        :param other_type:
        :return:
        """
        return True

    def __eq__(self, other: "DataType") -> bool:
        """
        All AnyType objects are equal
        :param other:
        :return:
        """
        return type(other) == type(self)


class Float64(DataType):
    """
    Type of the standard scalar data stream
    """
    scalar = True

    def can_convert_from(self, other_type: DataType) -> bool:
        """
        Return true if an input of this type can be read from_part an output of other_type
        :param other_type:
        :return:
        """
        return isinstance(other_type, self.__class__)

    def __eq__(self, other: "DataType") -> bool:
        """
        All Float64 types are equal
        :param other:
        :return:
        """
        return type(other) == type(self)


class Bus(DataType):
    """
    Type generated by ADCs and BusCreators
    """

    scalar = False

    def __init__(self, sub_types: Optional[Dict[str, DataType]] =None):
        """
        :param sub_types: A dictionary of channel name and DataType for the channel.
        """
        if sub_types is None:
            sub_types = {}
        self.sub_types = sub_types
        self.homogeneous = False

    def add_sub_type(self, channel_name: str, data_type: DataType) -> None:
        """
        Add another channel with data type to the bus type.
def __ne__(self, other):
        :param channel_name: Name of the new channel
        :param data_type: data type of the new channel
        :return:
        """
        if channel_name in self.sub_types:
            raise DuplicateNameError(f"The name '{channel_name}' was added at least twice to a bus.")
        self.sub_types[channel_name] = DataType

    def can_convert_from(self, other_type: DataType) -> bool:
        """
        A bus data type can be converted from_part another bus data type if all the channel names are in the other type
        and if the data type of the corresponding channels are also convertable.

        :param other_type:
        :return:
        """
        if not isinstance(other_type, self.__class__):
            return False
        for channel_name, data_type in self.sub_types.items():
            if not channel_name in other_type.sub_types:
                return False
            if not self.sub_types[channel_name].can_convert_from(other_type.sub_types[channel_name]):
                return False
        return True

    def __eq__(self, other: "DataType") -> bool:
        """
        Bus signal types are equal if all channel names are the same and have equal types
        :param other:
        :return:
        """
        if not isinstance(other, self.__class__):
            return False
        if len(self.sub_types) != len(other.sub_types):
            return False
        for channel_name, data_type in self.sub_types.items():
            if channel_name not in other.sub_types:
                return False
            if data_type != other.sub_types[channel_name]:
                return False
        return True


class Vector(DataType):
    """
    An array of values, not necessarily of the same type, though those types are handled specially as homogeneous vectors
    """

    scalar = False

    def __init__(self, sub_types: List[DataType]):
        """
        A list of subtypes.

        If the list is homogeneous, that will be detected and a homogeneous vector type will be created.
        If the list is a homogeneous list of a vector type, than a higher order homogeneous vector type will be created.
        :param sub_types:
        """
        # can't be mmpty
        if len(sub_types) == 0:
            raise InvalidDataType("Vector types cannot be of zero length")

        self.sub_types = sub_types

        # is homogeneous?
        test_type = sub_types[0]
        if not (test_type.scalar or test_type.homogeneous):
            self.homogeneous = False
        else:
            self.homogeneous = True
            for sub_type in sub_types:
                if sub_type != test_type:
                    self.homogeneous = False
                    break

        # determine shape if homogeneous
        if self.homogeneous:
            if not test_type.scalar:
                # find higher level (multi-dimensional) shape by adding the top dimension to the
                # shape of the sub_type
                self.shape = (len(sub_types), ) + test_type.shape
            else:
                self.shape = (len(sub_types), )
            self.sub_type = test_type
        else:
            self.shape = None
            self.sub_type = None


    def can_convert_from(self, other_type: DataType) -> bool:
        """
        A vector can convert from_part another vector if the lengths are the same
        and all subtypes are convertable.
        :param other_type:
        :return:
        """
        if not isinstance(other_type, self.__class__):
            return False
        if len(other_type.sub_types) != len(self.sub_types):
            return False

        if self.homogeneous and other_type.homogeneous:
            if self.shape != other_type.shape:
                return False
            if self.sub_type.can_convert_from(other_type.sub_type):
                return True
            else:
                return False

        for i,sub_type in enumerate(self.sub_types):
            if not sub_type.can_convert_from(other_type.sub_types[i]):
                return False

        return True

    def __eq__(self, other_type: DataType) -> bool:
        """
        A vector is equal to another vector if the lengths are the same
        and all subtypes are equal.
        :param other_type:
        :return:
        """
        if not isinstance(other_type, self.__class__):
            return False
        if len(other_type.sub_types) != len(self.sub_types):
            return False

        if self.homogeneous:
            if self.shape != other_type.shape:
                return False
            if not other_type.homogeneous:
                return False
            if self.sub_type == other_type.sub_type:
                return True
            else:
                return False

        if other_type.homogeneous:
            return False

        for i,sub_type in enumerate(self.sub_types):
            if not sub_type.can_convert_from(other_type.sub_types[i]):
                return False

        return True

    @classmethod
    def homogeneous_vector(cls, length: int, sub_type: DataType) -> "Vector":
        """
        Crate a homogeneous vector of the given length and sub_type
        :param length:
        :param sub_type:
        :return:
        """
        return cls([sub_type] * length)

    @classmethod
    def homogeneous_vector_of_scalars(cls, length: int) -> "Vector":
        """
        Crate a homogeneous vector of scalar values
        :param length:
        :return:
        """
        return cls([Float64()] * length)

class ArbitraryLengthVector(DataType):

    scalar = False

    def __init__(self, sub_type: DataType):
        self.sub_type = sub_type
        if sub_type.scalar or sub_type.homogeneous:
            self.homogeneous = True
            if sub_type.scalar:
                self.shape = ('*', )
            else:
                self.shape = ('*', ) + sub_type.shape
        else:
            self.homogeneous = False

    def can_convert_from(self, other_type: DataType) -> bool:
        if isinstance(other_type, self.__class__):
            # handle arbitrary length
            return self.sub_type.can_convert_from(other_type.sub_type)
        elif isinstance(other_type, Vector):
            if self.homogeneous and other_type.homogeneous:
                return self.sub_type.can_convert_from(other_type.sub_type)
            elif not (self.homogeneous or other_type.homogeneous):
                for st in other_type.sub_types:
                    if not self.sub_type.can_convert_from(st):
                        return False
                return True
        return False

    def __eq__(self, other_type: DataType) -> bool:
        if isinstance(other_type, self.__class__):
            # handle arbitrary length
            return self.sub_type == other_type.sub_type
        return False
