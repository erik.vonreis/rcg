from typing import Tuple, List, Dict, Any, Protocol
from .port import Port


class Part(Protocol):
    """
    Methods, values etc. that must be implemented by any part
    """

    # name of part in model, must be unique in the graph.
    name: str

    # input and output ports
    in_ports: List[Port]
    out_ports: List[Port]

    # True if code for the part is used to in the model calculation sequence
    calculable = True

