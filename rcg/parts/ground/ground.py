from ..data_types import Float64
from ..port import Port
from ..part import Part

class Ground(Part):
    def __init__(self, name: str):
        """
        :param name:
        """
        self.name = name
        self.in_ports = []
        self.out_ports = [Port(Float64)]

        self.calculable = False
