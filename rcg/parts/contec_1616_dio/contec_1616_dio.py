from ..data_types import Vector
from ..port import Port
from ..part import Part


class Contec1616Dio(Part):
    def __init__(self, name: str):
        """
        :param name:
        """
        self.name = name
        vec = Vector.homogeneous_vector_of_scalars(16)
        self.out_ports = [Port(vec)]
        self.in_ports = [Port(vec)]

