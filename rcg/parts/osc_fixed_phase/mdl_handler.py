from typing import TYPE_CHECKING
from .osc_fixed_phase import OscFixedPhase
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary
from ..mdl_helpers import get_mdl_object_name, get_parameters_from_description

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fp: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fp:
        :return:
        """
        if fp.name == 'Block' and fp.BlockType == 'Reference' and\
                fp.Tag is not None and fp.Tag == 'cdsOscFixedPhase':
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        
        name = get_mdl_object_name(obj_dict, "OscFixedPhase")
        params = get_parameters_from_description(obj_dict, "OscFixedPhase")
        return OscFixedPhase(name, params)


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
