from typing import TYPE_CHECKING
from .constant import Constant
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, MDLObjectListDictionary, check_one_of, MDLString, check_one_of_convertable
from ...compiler.compiler import FatalCompilationError
from ...errors import report_error

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fingerprint.name == 'Block' and fingerprint.BlockType == 'Constant':
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        try:
            name = check_one_of(obj_dict, 'Name', MDLString, str)
            value = check_one_of_convertable(obj_dict, 'Value', float)
        except Exception as e:
            report_error("failure to parse Constant")
            raise FatalCompilationError(f"A Constant failed to compile: {str(e)}")

        return Constant(name, value)


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
