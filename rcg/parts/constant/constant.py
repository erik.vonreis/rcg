from ..part import Part
from ..data_types import Float64
from ..port import Port
from ...errors import report_error


class Constant(Part):
    """
    Converts values to absolute values
    """
    def __init__(self, name: str, value: float):
        """

        :param name:
        """
        self.name = name
        self.value = value

        self.out_ports = [Port(Float64())]
        self.in_ports = []

        self.calculable = False
