from ..part import Part
from ..data_types import Float64, ArbitraryLengthVector
from ..port import Port


class Fcn(Part):
    """
    Matrix multiplication that takes mux input and gives output
    """
    def __init__(self, name: str, expr: str):
        """

        :param name:
        """
        self.name = name
        self.expr = expr

        self.out_ports = [Port(Float64())]
        self.in_ports = [Port(ArbitraryLengthVector(Float64()))]

