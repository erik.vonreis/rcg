from ..data_types import Float64
from ..port import Port
from ..part import Part


class EzCaWrite(Part):
    def __init__(self, name: str):
        """
        :param name:
        """
        self.name = name
        self.out_ports = []
        self.in_ports = [Port(Float64())]

