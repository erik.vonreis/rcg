from typing import Dict, TYPE_CHECKING

from rcg.parts.part import Part
from rcg.parts.data_types import Bus, Float64, DataType
from rcg.parts.port import Port

if TYPE_CHECKING:
    from ...compiler.compiler import Parameters


class Dac(Part):
    """
    Represent a 16-bit DAC
    """
    def __init__(self, name: str, params: 'Parameters', num_channels: int):
        """

        :param name:
        """

        self.name = name

        self.params = params

        # calculate outputs
        self.num_channels = num_channels

        f = Float64()

        self.out_ports = []
        self.in_ports = [Port(f) for i in range(self.num_channels)]
