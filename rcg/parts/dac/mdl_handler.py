from typing import TYPE_CHECKING
from .dac import Dac
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, MDLObjectList, check_one_of, MDLString
from ...compiler.compiler import FatalCompilationError, convert_parameter_list
from ...errors import report_error


if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fingerprint.name == 'Block' and fingerprint.BlockType == 'Reference' \
                and fingerprint.SourceBlock.startswith('dac/'):
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        

        try:
            name = check_one_of(obj_dict, 'Name', MDLString, str)
            param_list = check_one_of(obj_dict, 'Description', MDLString, str)
            ports = check_one_of(obj_dict, 'Ports', list, list)
        except Exception as e:
            raise FatalCompilationError(f"A DAC part failed to compile: {str(e)}")

        params = convert_parameter_list(param_list)

        num_channels = int(ports[0])

        if num_channels == 0:
            report_error(f"DAC '{name}' doesn't have any input ports")

        return Dac(name, params, num_channels)

# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
