from typing import Dict, Tuple

from rcg.compiler.compiler import FatalCompilationError
from rcg.compiler.mdl.item import MDLObject, MDLObjectList, check_one_of, MDLString, check_one_of_type


def get_mdl_object_name(obj_dict: dict, type_name: str) -> str:
    """
    For simple parts that only need a name from the object dictionary, get the name as a string.
    Raises a FatalCompilationError if the object doesn't have a subobject named 'Name'.

    :param obj_dict:
    :param type_name:
    :return: The name.
    """
    try:
        name = check_one_of(obj_dict, 'Name', MDLString, str)
    except Exception as e:
        raise FatalCompilationError(
            f"MDLObject, a {type_name}, failed to compile: {str(e)}")
    return name


def get_parameters_from_description(obj_dict: dict, type_name: str) -> Dict[str, str]:
    parameters = {}
    try:
        description = check_one_of_type(obj_dict, 'Description', MDLString, str, "")
    except Exception as e:
        raise FatalCompilationError(
            f"MDLObject, a {type_name}, failed to compile: {str(e)}")
    param_lines = description.split('\n,')
    for line in param_lines:
        line = line.strip()
        if line:
            param_name, param_value = line.split('=', 1)
            parameters[param_name.strip()] = param_value.strip()
    return parameters


def get_mdl_object_ports(obj_dict: dict, type_name: str) -> Tuple[int, int]:
    """
    Get the number of input ports and output ports as a tuple (in, out)
    :param obj_dict:
    :param type_name:
    :return:
    """
    try:
        ports = check_one_of(obj_dict, 'Ports', list, list)
        if len(ports) == 1:
            ports = ports + [0]
        match len(ports):
            case 2:
                ports = (int(ports[0]), int(ports[1]))
            case 1:
                ports = (int(ports[0]), 0)
            case 0:
                ports = (0, 0)
            case _:
                raise FatalCompilationError(f"Unrecognized number of port values: {len(ports)}")
    except Exception as e:
        raise FatalCompilationError(
            f"MDLObject, a {type_name}, failed to compile: {str(e)}")
    return ports