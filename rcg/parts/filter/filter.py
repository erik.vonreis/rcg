from ..data_types import Float64
from ..port import Port
from ..part import Part


class Filter(Part):
    """
    A standard filter module
    """
    def __init__(self, name: str):
        """
        :param name:
        """
        self.name = name
        self.in_ports = [Port(Float64())]
        self.out_ports = [Port(Float64())]

