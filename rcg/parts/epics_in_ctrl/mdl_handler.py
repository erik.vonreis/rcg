from typing import TYPE_CHECKING
from .epics_in_ctrl import EpicsInCtrl
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, MDLObjectList, check_one_of, MDLString
from ...compiler.compiler import FatalCompilationError
from ..mdl_helpers import get_mdl_object_name

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fp: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fp:
        :return:
        """
        if fp.name == 'Block' and fp.BlockType == 'Reference' and fp.Tag == 'cdsEpicsInCtrl':
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        
        name = get_mdl_object_name(obj_dict, "EpicsInCtrl")
        return EpicsInCtrl(name)


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
