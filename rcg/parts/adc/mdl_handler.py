from typing import TYPE_CHECKING
from .adc import Adc
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, MDLObjectListDictionary, check_one_of, MDLString
from ...compiler.compiler import FatalCompilationError, convert_parameter_list

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fingerprint.name == 'Block' and fingerprint.BlockType == 'Reference' \
                and fingerprint.SourceBlock.startswith('cdsAdc'):
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """

        try:
            name = check_one_of(obj_dict, 'Name', MDLString, str)
            param_list = check_one_of(obj_dict, 'Description', MDLString, str)
            source_block = check_one_of(obj_dict, 'SourceBlock', MDLString, str)
        except Exception as e:
            raise FatalCompilationError(f"An ADC failed to compile: {str(e)}")

        params = convert_parameter_list(param_list)

        # get ADC num from the source block
        adc_num = int(source_block.split('/', 1)[1][3:])

        return Adc(name, params, adc_num)

# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
