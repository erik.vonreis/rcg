from typing import Dict, TYPE_CHECKING

from rcg.parts.part import Part
from rcg.parts.data_types import Bus, Float64, DataType
from rcg.parts.port import Port

if TYPE_CHECKING:
    from ...compiler.compiler import Parameters


class Adc(Part):
    """
    Represent an ADC input part
    """
    def __init__(self, name: str, params: 'Parameters', adc_num: int):
        """

        :param name:
        """

        self.name = name

        self.params = params

        self.adc_num = adc_num
        # calculate outputs
        self.num_channels = 32

        chan_types: Dict[str, DataType] = {}
        f = Float64()
        for i in range(self.num_channels):
            chan_types[f"adc_{adc_num}_{i}"] = f

        self.out_ports = [Port(Bus(chan_types))]
        self.in_ports = []

        self.calculable = False
