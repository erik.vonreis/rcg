from dataclasses import dataclass
from .data_types import DataType

@dataclass
class Port:
    """Represents a single port, either input or output, on a part"""
    data_type: DataType
    linked: bool = False
