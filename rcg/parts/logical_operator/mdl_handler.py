from typing import TYPE_CHECKING
from .logical_operator import LogicalOperator
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, MDLObjectList, check_one_of, MDLString, check_one_of_type
from ...compiler.compiler import FatalCompilationError
from ...errors import report_error
from ..mdl_helpers import get_mdl_object_ports

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fingerprint.name == 'Block' and fingerprint.BlockType == 'Logic':
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        
        try:
            name = check_one_of(obj_dict, 'Name', MDLString, str)
            operator = check_one_of_type(obj_dict, 'Operator', MDLString, str)
        except Exception as e:
            report_error("failure to parse LogicalOperator")
            raise FatalCompilationError(f"A LogicalOperator, failed to compile: {str(e)}")

        ports = get_mdl_object_ports(obj_dict, "LogicalOperator")

        return LogicalOperator(name, operator, ports[0])


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
