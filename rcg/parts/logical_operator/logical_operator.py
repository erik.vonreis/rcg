from ..part import Part
from ..data_types import Float64
from ..port import Port
from ...errors import report_error


class LogicalOperator(Part):
    """
    Converts values to absolute values
    """
    def __init__(self, name: str, operator: str, num_inputs: int):
        """

        :param name:
        """
        self.name = name
        self.operator = operator
        self.num_inputs = num_inputs

        self.in_ports = [Port(Float64())] * num_inputs
        self.out_ports = [Port(Float64())]

