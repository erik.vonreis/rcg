from typing import TYPE_CHECKING
from .sum import Sum
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ...compiler.mdl.item import MDLObjectListDictionary, MDLObjectList, check_one_of, MDLString, check_one_of_type
from ...compiler.compiler import FatalCompilationError


if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fingerprint: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fingerprint.name == 'Block' and fingerprint.BlockType == 'Sum':
            return True
        else:
            return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        
        try:
            name = check_one_of(obj_dict, 'Name', MDLString, str)
            ports = check_one_of_type(obj_dict, 'Ports', list, list)
            inputs = check_one_of(obj_dict, 'Inputs', MDLString, str)
        except Exception as e:
            raise FatalCompilationError(f"A sum part failed to compile: {str(e)}")

        num_inputs = int(ports[0])

        # some products just have a number in text format for the inputs
        try:
            input_value = int(inputs)
            inputs = "+" * input_value
        except ValueError:
            pass

        return Sum(name, num_inputs, inputs)


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
