from ..part import Part
from ..data_types import Float64
from ..port import Port
from ...errors import report_error


class Sum(Part):
    def __init__(self, name: str, num_inputs: int, inputs: str):
        """

        :param name:
        """
        self.name = name
        self.num_inputs = num_inputs
        self.inputs = "".join([x for x in inputs if x != "|"])

        # there's an extra char at the start to make the indexes 1 based for some reason
        if len(self.inputs) != self.num_inputs:
            report_error("Number of inputs do not match assignment of inputs for a Sum part")

        self.out_ports = [Port(Float64())]
        self.in_ports = [Port(Float64())] * self.num_inputs

