from ..port import Port
from ..data_types import AnyType
from ..part import Part
from ..delay_output.delay_output import DelayOutput


class Delay(Part):
    def __init__(self, name: str):
        self.name = name
        self.in_ports = [Port(AnyType())]
        self.out_ports = []
        self.delay_output = DelayOutput(name, self)