from typing import TYPE_CHECKING
from .delay import Delay
from ...compiler.mdl.mdl_handler_protocol import MDLHandlerProtocol
from ..mdl_helpers import get_mdl_object_name

if TYPE_CHECKING:
    from ...compiler.mdl.object_fingerprint import ObjectFingerprint
    from ...compiler.mdl.item import MDLObjectListDictionary
    from ..part import Part


class MDLHandler(MDLHandlerProtocol):
    @staticmethod
    def object_filter(fp: "ObjectFingerprint") -> bool:
        """
        Return true if MDL object could be translated into a Delay part
        :param fingerprint:
        :return:
        """
        if fp.name == "Block" and fp.BlockType == "UnitDelay":
            return True
        return False

    @staticmethod
    def object_converter(obj_dict: "MDLObjectListDictionary") -> "Part":
        """
        Return a Delay part for the given mdl_object
        :param mdl_object:
        :return:
        """
        
        name = get_mdl_object_name(obj_dict, "Delay")
        return Delay(name)


# let static analysis check this type
m: MDLHandlerProtocol = MDLHandler()
